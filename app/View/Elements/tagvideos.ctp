<div class="row" id="free-video-content">
  <div class="large-11 columns">   
	<!-- Begin Repeat this section for each category -->    
	<div class="section-container tabs" data-section="">
	  <section class="section border-catergory">
		<h2 class="tag-name"><?php echo $tag; ?></h2>
		<div class="content" data-slug="panel2">
		  <div class="large-block-grid-4" id="homepageItems">
		  
		  	<?php 
				foreach($tagvideos as $video):
			?>
			<div class="item">
			  <div class="thumb"> 
              
              <?php 
			  	echo $this->Html->image($video->thumbnails->thumbnail[1]->_content, array(
					"alt" => $video->title,
					'url' => array('controller' => 'Videos', 'action' => 'view', $video->id)));
				echo '<a href="/islandvideo/Videos/view/'.$video->id.'"><span class="vertical-align"></span> </span> <span class="overlay"></span></a>';
				echo '<span class="duration">'.gmdate("H:i:s", $video->duration).'</span>';
			  ?>
              </div>
			  <div class="data">
				<h2 class="entry-title font_bold font_bold">
                	<?php echo $this->Html->link($this->Text->truncate($video->title, 70, array('ellipsis' => '...', 'exact' => false)),
array('controller' => 'videos', 'action' => 'view', $video->id), array('title' => $video->title)); ?></h2>
			  </div>
			</div>
			<?php endforeach; ?>
			
		  </div>
		  <div class="holder"></div>
		</div>
	  </section>
	</div>
	<!-- End Repeat this section for each category -->
	
  </div>
  <!-- End Contact Details -->
  <!-- Sidebar -->
<?php echo $this->element('mostviewed'); ?>
  
  <!-- End Sidebar -->
</div>