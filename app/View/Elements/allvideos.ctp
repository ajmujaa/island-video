<div class="row">
    <div class="large-16 columns">  
    <h2>All Videos</h2>
    	<div class="slider5">
          <?php foreach($allvideos as $video): ?>
          <div class="slide">
			  <?php 
			  	echo $this->Html->image($video->thumbnails->thumbnail[1]->_content, array(
					"alt" => $video->title,
					'url' => array('controller' => 'Videos', 'action' => 'view', $video->id)));
				echo '<a href="/islandvideo/Videos/view/'.$video->id.'"><span class="vertical-align"></span> </span> <span class="overlay"></span></a>';
			  ?>
              <p class="realted-item-header"><?php echo $this->Html->link($this->Text->truncate($video->title, 20, array('ellipsis' => '...', 'exact' => false)),
array('controller' => 'videos', 'action' => 'view', $video->id), array('title' => $video->title)); ?></p>
              <a href="#" class="realted-item-link">Category Link</a>
          </div>
          <?php endforeach; ?>
        </div>
    </div>
</div>