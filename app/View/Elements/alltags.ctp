<div class="large-5 columns">
	<!--Begin Repeat this for left vid colums items -->
	<div class="section-container tabs" data-section="">
	  <section class="section">
		<h2>Tags</h2>
		<div class="content" data-slug="panel2">
		<div class="large-block-grid-3">
			<div class="tags">
            	<?php 
					$output = array();
					foreach($tags as $tag):
						$output[] = '<span>'.$this->Html->link($tag, array('controller' => 'tags', 'action' => 'view', $tag), array('title' => $tag)).'</span>';
					endforeach;
					echo implode(' ', $output); 
				?>
            </div>
		  </div>
		</div>
	  </section>
	</div>    
	<!--End Repeat this for left vid colums items -->
</div>