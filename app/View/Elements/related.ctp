<div class="row">
<div class="large-16 columns">   
    <!-- Begin Repeat this section for each category -->    
    <div data-section="" class="section-container tabs">
      <section class="section border-similar-categories">
        <h2>Related Items</h2>
        <div data-slug="panel2" class="content">
          <div class="large-block-grid-12 slider5">
          	<?php foreach($similarvideos as $video): ?>
            <div class="item slide">
              <div class="thumb"> 
              	<?php 
					echo $this->Html->image($video->thumbnails->thumbnail[1]->_content, array(
					"alt" => $video->title,
					'url' => array('controller' => 'Videos', 'action' => 'view', $video->id)));
				?>
              </div>
              <div class="data">
                <h2 class="entry-title font_bold font_bold">
                	<?php echo $this->Html->link($this->Text->truncate($video->title, 40, array('ellipsis' => '...', 'exact' => false)),
array('controller' => 'videos', 'action' => 'view', $video->id), array('title' => $video->title)); ?>
                </h2>
              </div>
            </div>
			<?php endforeach; ?>
          </div>
        </div>
      </section>
    </div>
    <!-- End Repeat this section for each category -->
    
  </div>
</div>