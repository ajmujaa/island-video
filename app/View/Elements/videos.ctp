<div class="row" id="free-video-content">
  <div class="large-11 columns">   
	<!-- Begin Repeat this section for each category -->    
	<div class="section-container tabs" data-section="">
      <?php foreach($channels as $channel): ?>	
	  <section class="section border-catergory">
          <?php if(!empty($channelVideos)){ ?>
		<h2><a class="main-category-title"><?php echo $channel['Channel']['Name']; ?></a></h2>
		<div class="content" data-slug="panel2">
		  <div class="large-block-grid-4 slider">		  
		  	<?php 
				foreach($channelVideos as $video):
				if($video->channel_id === $channel['Channel']['Id']){
			?>
			<div class="item slide">
            	<?php echo $video->type == "Premium" ? '<div class="ribbon"></div>' : ''; ?>
			  <div class="thumb"> 
              <?php 
			  	echo $this->Html->image($video->thumbnails->thumbnail[1]->_content, array(
					"alt" => $video->title,
					'url' => array('controller' => 'Videos', 'action' => 'view', $video->id)));
				echo '<a href="/islandvideo/Videos/view/'.$video->id.'"><span class="vertical-align"></span> </span> <span class="overlay"></span></a>';
				echo '<span class="duration">'.gmdate("H:i:s", $video->duration).'</span>';
			  ?>
              </div>
			  <div class="data">
				<h2 class="entry-title font_bold font_bold">
                	<?php echo $this->Html->link($this->Text->truncate($video->title, 70, array('ellipsis' => '...', 'exact' => false)),
array('controller' => 'videos', 'action' => 'view', $video->id), array('title' => $video->title)); ?> 
<?php 
	if($video->author_id > 0){
		echo '<strong>by</strong> ' . $this->Html->link($video->author_name, array('controller' => 'videos', 'action' => 'author', $video->author_id), array('title' => $video->author_name));	
	}
?></h2>
			  </div>
			</div>
			<?php }endforeach; ?>
			
		  </div>
		</div>
          <?php } ?>
	  </section>
      <?php endforeach; ?>
	</div>
	<!-- End Repeat this section for each category -->
	
  </div>
  <!-- End Contact Details -->
  <!-- Sidebar -->
<?php echo $this->element('mostviewed'); ?>
<?php echo $this->element('alltags'); ?>  
  <!-- End Sidebar -->
</div>