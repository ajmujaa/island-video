<div class="row display-none" id="paid-video-content" style="display: block;">
<div class="large-11 columns">   
    <!-- Begin Repeat this section for each category -->    
    <div class="section-container tabs" data-section="">
      <section class="section border-catergory">
        <div class="content categories" data-slug="panel2">
          	<?php 
				$i = 0;
				foreach($channels as $channel):
			?>
            <h1><?php echo $channel['Channel']['Name']; ?><select class="categ" id="<?php echo 'categ'.($i + 1); ?>"></select></h1>
            <div class="large-block-grid-4 items slidera" id="<?php echo 'items'.($i + 1); ?>">
            <?php foreach($groupChannels as $groupChannel):
					if($groupChannel['ChannelsGroupsAlbum']['Channel_Id'] === $channel['Channel']['Id']){
                        foreach($filteredGroups as $group):
                            if($group[0] === $groupChannel['ChannelsGroupsAlbum']['Group_Id']){                               
			?>
            <div class="item slidea">
			  <div class="thumb"> 
              <?php 
			  	echo $this->Html->image('http://b.vimeocdn.com/ts/463/424/463424023_200.jpg', array(
					"alt" => $group[1],
					'url' => array('controller' => 'Groups', 'action' => 'view', '?' => array('group_id' => $group[0], 'channel_id' => $channel['Channel']['Id']))));
				echo '<a href="/islandvideo/Groups/view?group_id='.$group[0].'&channel_id='.$channel['Channel']['Id'].'"><span class="vertical-align"></span> </span></a>';
			  ?>
              </div>
			  <div class="data">
				<h2 class="entry-title font_bold font_bold">
                	<?php echo $this->Html->link($this->Text->truncate($group[1], 70, array('ellipsis' => '...', 'exact' => false)),
array('controller' => 'Groups', 'action' => 'view', '?' => array('group_id' => $group[0], 'channel_id' => $channel['Channel']['Id'])), array('title' => $group[1])); ?></h2>
			  </div>
            </div>
            <?php 
                            }
                        endforeach;
                        } 
                    endforeach; ?>
            
            </div>
            <div class="holder multiHolder"></div>
			<?php $i++;
				 endforeach; ?>
        </div>
      </section>
    </div>
    <!-- End Repeat this section for each category -->
    
  </div>
  <!-- End Contact Details -->
  <!-- Sidebar -->
<?php echo $this->element('mostviewed'); ?>
</div>