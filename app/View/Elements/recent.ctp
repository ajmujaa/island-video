<div class="large-5 columns">
  
   <!--Begin Repeat this for left vid colums items -->
    <div class="section-container tabs" data-section>
      <section class="section">
        <h2><a class="right-category-title">Recent Additions </a></h2>
        <div class="content" data-slug="panel2">
        <div class="large-block-grid-3">
          <?php foreach($recent as $item): ?>
            <div class="item">
              <div class="thumb"> 
              	<?php echo $this->Html->image($item->thumbnails->thumbnail[1]->_content, array(
					"alt" => $item->title,
					'url' => array('controller' => 'Videos', 'action' => 'view', $item->id)));
				echo '<a href="/islandvideo/Videos/view/'.$item->id.'"><span class="vertical-align"></span> </span> <span class="overlay"></span></a>' ?>
              </div>
              <div class="data right padding-left5">
                <h2 class="entry-title font_bold font_bold">
					<?php echo $this->Html->link($this->Text->truncate($item->title, 45, array('ellipsis' => '...', 'exact' => false)),
array('controller' => 'videos', 'action' => 'view', $item->id), array('title' => $item->title)); ?>	
				</h2>
                <h3 class="added-date">
                	<?php 
						echo CakeTime::timeAgoInWords($item->upload_date, array(
							'accuracy' => array('month' => 'month'),
							'end' => '1 year'
						));
					?> </h3>
              </div>
            </div>
            <?php endforeach; ?>
           
            
          </div>
        </div>
      </section>
    </div>    
    <!--End Repeat this for left vid colums items -->
  </div>