<!-- End Main Content and Sidebar -->
<!-- Footer -->
<div class="row">
  <footer id="colophon">
    <div class="large-16 columns">
      <hr>
      <div class="social-nav"> <span class="desc">Follow us elsewhere</span>
        <ul>
          <li class="twitter"><a title="Follow us on Twitter" href="#">Follow us on Twitter</a></li>
          <li class="facebook"><a title="Become a fan on Facebook" href="#">Become a fan on Facebook</a></li>
          <li class="gplus"><a title="Follow us on Google Plus" href="#">Follow us on Google Plus</a></li>
          <li class="rss"><a title="Subscriber to RSS Feed" href="#">Subscriber to RSS Feed</a></li>
        </ul>
      </div>
      <div class="footer-nav">
        <ul class="menu">
          <li class=""><a href="#">Home</a></li>
          <li class=""><a href="#">About</a></li>
          <li class=""><a href="#">Free Videos</a></li>
          <li class=""><a href="#">Videos</a></li>
          <li class=""><a href="#">Categories</a></li>
          <li class=""><a href="#">Contact</a></li>
        </ul>
      </div>
      <div>
        <p class="footer-copyright">© Copyright no one at all. Go to town.</p>
      </div>
    </div>
  </footer>
</div>
<!-- End Footer -->
</div>