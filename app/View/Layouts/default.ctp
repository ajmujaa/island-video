<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title><?php echo $title_for_layout; ?></title>
	<?php
		echo $this->Html->css('foundation');
		echo $this->Html->css('font-awesome');
		echo $this->Html->css('jquery.bxslider');
		echo $this->Html->css('animate');
		echo $this->Html->css('jPages');
		echo $this->Html->css('style');		
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
    <!--[if lt IE 9]>
      <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.2/html5shiv.js"></script>
      <script src="//s3.amazonaws.com/nwapi/nwmatcher/nwmatcher-1.2.5-min.js"></script>
      <script src="//html5base.googlecode.com/svn-history/r38/trunk/js/selectivizr-1.0.3b.js"></script>
      <script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.1.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<div id="container">
    	<?php echo $this->element('header'); ?>
        <?php echo $this->element('topbar'); ?>
		<div id="content">
			<?php echo $this->Session->flash(); ?>

			<?php echo $this->fetch('content'); ?>
		</div>
        <?php echo $this->element('footer'); ?>
	</div>
    <?php 
		echo $this->Html->script('modernizr');
		echo $this->Html->script('jquery-1.10.2.min');
		echo $this->Html->script('foundation.min');
		echo $this->Html->script('jquery.bxslider.min');
		echo $this->Html->script('highlight.pack');
		echo $this->Html->script('tabifier');		
		echo $this->Html->script('jPages.min');
		echo $this->Html->script('rem.min');
		echo $this->Html->script('application');
	?>
</body>
</html>
