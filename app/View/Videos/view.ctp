<?php App::uses('CakeTime', 'Utility'); ?>
<div class="row">


  <div class="large-11 columns">   
    <!-- Begin Item content display -->    

      <section class="video section border-catergory">
        <h2 class="video-title"><?php echo $video[0]->title; ?></h2>
        <div class="content" data-slug="panel2">
          <div class="large-block-grid-16">
            <?php if($video[0]->type === "Premium" && ($video[0]->token <= $usertoken)){ ?>
            <div class="video-overlay" data-access="access">
                <div class="warning-msg">
                    <div class="warning warning-yellow"></div>
                    <div class="msg">This is a Premium video and <?php echo $video[0]->token; ?> tokens will be deducted from your account. Do you want to continue?</div>
                </div>
                <div class="warning-btn"><button class="yes" id="yes" data-id="<?php echo $video[0]->id; ?>">Yes, Please</button><button class="no">No, Thanks</button></div>	
            </div>
            <?php }else if($video[0]->token > $usertoken){ ?>
	        <div class="video-overlay" data-access="noaccess">
                <div class="warning-msg">
                    <div class="warning warning-yellow"></div>
                    <div class="msg">You do not have enough tokens to watch this video or you are not logged in. Please buy more tokens or login to your account</div>
                </div>
                <div class="warning-btn"></div>	
            </div>	
			<?php } ?>
            <div class="flex-video widescreen vimeo"> 
            	<iframe id="largeplayer" src="http://player.vimeo.com/video/<?php echo $video[0]->id; ?>?title=0&portrait=0&byline=0&badge=0" width="400" height="225" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen>
                </iframe> 
            </div>       
            
          </div>
          
          <div class="large-block-grid-16">
          	<div class="tags">
            	<?php 
					$output = array();
					foreach($video[0]->tags->tag as $tag):
						$output[] = '<span>'.$this->Html->link($tag->normalized, array('controller' => 'tags', 'action' => 'view', $tag->normalized), array('title' => $tag->normalized)).'</span>';
					endforeach;
					echo implode(' ', $output); 
				?>
            </div>
            <div class="description">
            	<p><?php echo $video[0]->description; ?></p>
                <p>Video token value: <?php echo $video[0]->token; ?></p>
                
            </div>
            <div class="description">
				<?php 
                    if($video[0]->author_id > 0){
                        echo '<strong>Published by:</strong> ' . $this->Html->link($video[0]->author_name, array('controller' => 'videos', 'action' => 'author', $video[0]->author_id), array('title' => $video[0]->author_name));	
                    }
					echo " ";
					echo CakeTime::timeAgoInWords($video[0]->upload_date, array(
							'accuracy' => array('month' => 'month'),
							'end' => '1 year'
						));
                ?>
            </div>
          </div>
          
        </div>
      </section>
    <!-- End Item content display -->
    
  </div>
  <!-- End Contact Details -->
  
  
  
  
  <!-- Sidebar -->
<?php echo $this->element('similar'); ?>
  <!-- End Sidebar -->
<!--Begin similar items-->
<?php echo $this->element('related'); ?>
<!--End similar items-->
<!-- Begin related items block -->
<?php echo $this->element('allvideos'); ?>
<!-- End realted items block -->
</div>