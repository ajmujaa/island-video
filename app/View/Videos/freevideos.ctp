<?php App::uses( 'CakeTime', 'Utility'); ?>
<div class="row" style="display: block;">
    <div class="large-11 columns">
        <!-- Begin Repeat this section for each category -->
        <div class="section-container tabs" data-section="">
            <section class="section border-catergory">
                <h2><a class="main-category-title">Free Videos</a></h2>
                <div class="content" data-slug="panel2">
                    <div class="large-block-grid-4" id="homepageItems">
                        <?php
                                if(!empty($videos)){ 
                                    foreach($videos as $video): 
                        ?>
                        <div class="item">
                            <div class="thumb">
                                <?php echo $this->Html->image($video[0]->thumbnails->thumbnail[0]->_content, array( "alt" => $video[0]->title, 'url' => array('controller' => 'Videos', 'action' => 'view', $video[0]->id))); ?>
                            </div>
                            <div class="data">
                                <h2 class="entry-title font_bold font_bold">
                	               <?php echo $this->Html->link($this->Text->truncate($video[0]->title, 70, array('ellipsis' => '...', 'exact' => false)),
array('controller' => 'Videos', 'action' => 'view', $video[0]->id), array('title' => $video[0]->title)); ?> <?php 
                                        if($video[0]->author_id > 0){
                                            echo '<strong>by</strong> ' . $this->Html->link($video[0]->author_name, array('controller' => 'Videos', 'action' => 'author', $video[0]->author_id), array('title' => $video[0]->author_name));	
                                        }
                                    ?>
                                </h2>
                            </div>
                        </div>
                        <?php endforeach; } else { echo "No videos found"; } ?>
                    </div>
                    <div class="holder"></div>
                </div>
            </section>
        </div>
        <!-- End Repeat this section for each category -->

    </div>
    <!-- End Contact Details -->
    <!-- Sidebar -->
    <?php echo $this->element('mostviewed'); ?>
</div>
