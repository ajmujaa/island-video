<?php App::uses( 'CakeTime', 'Utility'); ?>
<div class="row" id="free-video-content">
    <div class="large-11 columns">
        <!-- Begin Repeat this section for each category -->
        <div class="section-container tabs" data-section="">
            <section class="section border-catergory">
                <h2><a class="main-category-title">Subcategory - <?php echo $albumName; ?></a></h2>
                <div class="content" data-slug="panel2">
                    <div class="large-block-grid-4" id="homepageItems">

                        <?php $videos=array_filter($videos); if(!empty($videos)){ foreach($videos as $video): ?>
                        <div class="item">
                            <div class="thumb">
                                <?php echo $this->Html->image($video[0]->thumbnails->thumbnail[0]->_content, array( "alt" => $video[0]->title, 'url' => array('controller' => 'Videos', 'action' => 'view', $video[0]->id))); ?>
                            </div>
                            <div class="data">
                                <h2 class="entry-title font_bold font_bold">
                	<?php echo $this->Html->link($this->Text->truncate($video[0]->title, 70, array('ellipsis' => '...', 'exact' => false)),
array('controller' => 'Videos', 'action' => 'view', $video[0]->id), array('title' => $video[0]->title)); ?> <?php 
	if($video[0]->author_id > 0){
		echo '<strong>by</strong> ' . $this->Html->link($video[0]->author_name, array('controller' => 'Videos', 'action' => 'author', $video[0]->author_id), array('title' => $video[0]->author_name));	
	}
?>
                  </h2>
                            </div>
                        </div>
                        <?php endforeach; }else{ echo "No videos found"; } ?>
                    </div>
                    <div class="holder"></div>


                </div>
                <?php if(!empty($authors)){ ?>
                <div class="large-block-grid-4" id="authors">
                    <h2>Authors</h2>
                    <?php foreach($authors as $author): ?>
                    <div class="item">
                        <div class="thumb">
                            <?php echo $this->Html->image('http://images.boomsbeat.com/data/images/full/595/bill-gates-jpg.jpg', array( "alt" => $author['FullName'], 'url' => array('controller' => 'Videos', 'action' => 'author', $author['Id']))); ?>
                        </div>
                        <div class="data">
                            <h2 class="entry-title font_bold font_bold">
                	<?php echo $this->Html->link($this->Text->truncate($author['FullName'], 70, array('ellipsis' => '...', 'exact' => false)),
array('controller' => 'Videos', 'action' => 'author', $author['Id']), array('title' => $author['FullName'])); ?> <?php 
	if($author['FullName'] > 0){
		echo '<strong>by</strong> ' . $this->Html->link($author['FullName'], array('controller' => 'Videos', 'action' => 'author', $author['Id']), array('title' => $author['FullName']));	
	}
?>
                  </h2>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
                <?php } ?>

            </section>



        </div>
        <!-- End Repeat this section for each category -->

    </div>
    <!-- End Contact Details -->
    <!-- Sidebar -->
    <?php echo $this->element('mostviewed'); ?>

    <!-- End Sidebar -->
</div>