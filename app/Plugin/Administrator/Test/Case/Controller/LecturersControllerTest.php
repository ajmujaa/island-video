<?php
App::uses('LecturersController', 'Administrator.Controller');

/**
 * LecturersController Test Case
 *
 */
class LecturersControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'plugin.administrator.lecturer'
	);

}
