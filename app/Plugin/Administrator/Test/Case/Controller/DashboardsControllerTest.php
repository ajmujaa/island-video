<?php
App::uses('DashboardsController', 'Administrator.Controller');

/**
 * DashboardsController Test Case
 *
 */
class DashboardsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'plugin.administrator.dashboard'
	);

}
