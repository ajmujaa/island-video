<?php
App::uses('VideosController', 'Administrator.Controller');

/**
 * VideosController Test Case
 *
 */
class VideosControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'plugin.administrator.video'
	);

}
