<?php
App::uses('AdministratorAppController', 'Administrator.Controller');
App::import('Vendor', 'Vimeo/phpVimeo');
/**
 * Videos Controller
 *
 */
class VideosController extends AdministratorAppController {

/**
 * Scaffold
 *
 * @var mixed
 */
	public $scaffold;
	public $helpers = array('Html', 'Form');
	public $components = array('VimeoAuth', 'Session', 'Category');
	
		
	public function index(){
		$this->set('title_for_layout', 'Administrator | Videos');				
		$this->set('videos', $this->getAllVimeoVideos());
		$this->notifyVideoCount();
	}
	
	public function addAll(){	
		$videos = $this->getAllVimeoVideos();
		foreach($videos as $video):
			$isExist = $this->Video->findById($video->id);
			$id = $video->id;
			if(empty($isExist)){
				$this->Video->create();
				$this->request->data['Video']['Id'] = $id;
				$this->request->data['Video']['CategoryId'] = '1';
				$this->request->data['Video']['SubCategoryId'] = '1';
				$this->request->data['Video']['VideotypeId'] = '1';
				$this->request->data['Video']['CreatedDate'] = date("Y-m-d H:i:s");
				$this->request->data['Video']['UpdatedDate'] = date("Y-m-d H:i:s");
				$this->Video->save($this->request->data);	
			}
			
		endforeach;
		$this->redirect(array('action' => 'index'));
	}
	
	public function view($id = null){		
		if(!$id){
			throw new NotFoundException(__('Invalid Video'));	
		}
		$video = $this->Video->findById($id);
		if(!$video){
			throw new NotFoundException(__('Invalid Video'));
		}
		$this->set('videodata', $video);
		$this->set('video', $this->getVimeoVideo($id));
		
	}
	
	
	public function edit($id = null){
		$this->loadModel('GroupsVideo');
		$this->loadModel('AlbumsVideo');
		if(!$id){
			throw new NotFoundException(__('Invalid Video'));	
		}
		$videodata = $this->Video->findById($id);
		$videogroups = $this->GroupsVideo->find('all', array('conditions' => array('VideoId' => $id)));
		$videoalbums = $this->AlbumsVideo->find('all', array('conditions' => array('VideoId' => $id)));
		if(!$videodata){
			throw new NotFoundException(__('Invalid Video'));
		}
		$grouparray = array();
		$albumarray = array();
		foreach($videogroups as $videogroup){
			$grouparray[] = $videogroup['GroupsVideo']['GroupId'];	
		}
		
		foreach($videoalbums as $videoalbum){
			$albumarray[] = $videoalbum['AlbumsVideo']['AlbumId'];	
		}
		$this->set('video', $this->getVimeoVideo($id));
		$this->set('group', $grouparray);
		$this->set('album', $albumarray);
		$this->listAllLecturers();
		$this->listAllVideoTypes();	
		$this->getAllChannels();
		$this->getAllGroups();
		$this->getAllAlbums();        
		if($this->request->is('post') || $this->request->is('put')){
			$this->Video->id = $id;
			$this->request->data['Video']['UpdatedDate'] = date("Y-m-d H:i:s");
			if($this->Video->save($this->request->data)){
                $this->loadModel('GroupsVideo');
                $this->loadModel('AlbumsVideo');
                $this->GroupsVideo->query("DELETE FROM groups_videos WHERE VideoId = '".$id."'");
                $this->AlbumsVideo->query("DELETE FROM albums_videos WHERE videoId = '".$id."'");
                
				foreach($this->request->data['Video']['group'] as $group){
					$this->updateVideoGroups($id, $group);
				}
				foreach($this->request->data['Video']['album'] as $album){
					$this->updateVideoAlbums($id, $album);
				}
				$this->Session->setFlash(__('video has been Updated.'));
				$this->redirect(array('action' => 'index'));	
			}else {
                $this->Session->setFlash(__('Unable to add Video.'));
            }
		}	
		if(!$this->request->data){
			$this->request->data = $videodata;	
		}
	}
	
	private function updateVideoGroups($videoId, $groupId){
		$this->loadModel('GroupsVideo');
		$this->GroupsVideo->query("INSERT INTO groups_videos (VideoId, GroupId) VALUES (".$videoId.", ".$groupId.")");
        
	}	
	
	private function updateVideoAlbums($videoId, $albumId){
		$this->loadModel('AlbumsVideo');
        $this->AlbumsVideo->query("INSERT INTO albums_videos (VideoId, AlbumId) VALUES (".$videoId.", ".$albumId.")");
	}
	
	
	
	private function listAllLecturers(){
		 $this->set('leturers',$this->Video->User->find('list', array('conditions' => array('User.RoleId' => '3'), 'fields' => array('Id','FullName'))));
	}
	
	private function listAllVideoTypes(){
		$this->set('videoTypes', $this->Video->Videotype->find('list', array('fields' => array('Id', 'Type'))));	
	}
	
	private function getAllChannels(){
		$this->loadModel('Channel');
		$this->set('channels', $this->Category->getRequestedList('Channel', 'Name'));	
	}
	
	private function getAllGroups(){
		$this->loadModel('Group');
		$this->set('groups', $this->Category->getRequestedList('Group', 'Name'));
	}
	
	private function getAllAlbums(){
		$this->loadModel('Album');
		$this->set('albums',$this->Category->getRequestedList('Album', 'Name'));
	}
	
	private function notifyVideoCount(){
		$vimeo = $this->VimeoAuth->doAuth();
		$vimeovideos = $vimeo->call('vimeo.videos.getAll', array('user_id' => $this->VimeoAuth->getUsername(), 'summary_response' => true));
		$localvideos = $this->Video->find('count');
		$vimeovideocount = intval($vimeovideos->videos->total);
		if($localvideos < $vimeovideocount){
			$this->Session->setFlash(__('New videos have been added to your Vimoe account. Please update your database'));	
		}
	}
	
	
	private function getAllVimeoVideos(){
		$vimeo = $this->VimeoAuth->doAuth();
		$videos = $vimeo->call('vimeo.videos.getAll', array('user_id' => $this->VimeoAuth->getUsername(), 'sort' => 'newest', 'full_response' => true));
		return $videos->videos->video;	
	}
	
	private function getVimeoVideo($id = null){
		$vimeo = $this->VimeoAuth->doAuth();
		$video = $vimeo->call('vimeo.videos.getInfo', array('video_id' => $id));
		return $video->video;
	}
	
}
