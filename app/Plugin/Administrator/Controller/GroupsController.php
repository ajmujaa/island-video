<?php 
	App::uses('AdministratorAppController', 'Administrator.Controller');
	
	class GroupsController extends AdministratorAppController{
		
		public $scaffold;
		public $helpers = array('Html', 'Form');
		public $components = array('Session', 'Category');
		
		public function index(){
			$this->set('title_for_layout', 'Administrator | Groups');	
            $groups = $this->Category->getAllGroupsUnfiltered();
			$channelName = '';
			$groupName = '';
			$channelsGroup = array();
			foreach($groups as $group){
                if($group['ChannelsGroupsAlbum']['Channel_Id'] !== null){
                    $channelName = $this->Category->getChannelName($group['ChannelsGroupsAlbum']['Channel_Id']);
                    $groupName = $this->Category->getGroupName($group['ChannelsGroupsAlbum']['Group_Id']);
                    $channelsGroup[] = array($group['ChannelsGroupsAlbum']['Group_Id'], $group['ChannelsGroupsAlbum']['Channel_Id'],$groupName, $channelName);   
                }
			}
			$this->set('groups', $this->Category->object_array_unique($channelsGroup));
		}
		
		public function view($id=null){
			$this->set('title_for_layout', 'Administrator | View Groups');				
			if(!$id){
				throw new NotFoundException(__('Invalid Group'));	
			}
			$group = $this->Category->getRequestedItem('Group', $id);
			if(!$group){
				throw new NotFoundException(__('Invalid Group'));
			}			
			$this->set('group', $group);
		}
		
		public function edit($id = null){
            $this->loadModel('ChannelsGroup');
			$this->set('title_for_layout', 'Administrator | Edit Group');	
			$data = $this->request->query;
			if(!$data['id']){
				throw new NotFoundException(__('Invalid Channel'));	
			}
			$this->set('channelId', $data['channelId']);
			$channel = $this->Category->getRequestedItem('Group', $data['id']);
			$this->set('channels', $this->Category->getRequestedList('Channel', 'Name'));
			
			if($this->request->is('post') || $this->request->is('put')){	
                $rdata = $this->request->data['Group'];
                $this->updateGroupName($rdata['Id'], $rdata['Name']);
                $this->ChannelsGroup->query("UPDATE channels_groups_albums SET Channel_Id = ".$rdata['ChannelId']." WHERE Channel_Id = ".$data['channelId']." AND  Group_Id = ".$data['id']."");
                $this->Session->setFlash(__('Group has been Updated.'));
				$this->redirect(array('action' => 'index', '?' => array('type' => $this->request->data['Group']['Name'])));	
			}	
			if(!$this->request->data){
				$this->request->data = $channel;	
			}
			
		}
		
		public function add(){
			$this->set('channels', $this->Category->getRequestedList('Channel', 'Name'));
			if($this->request->is('post')){
				$this->Group->create();
                
                $this->Group->save($this->request->data);
                $lastGroupId = $this->Group->getLastInsertId();
                $this->loadModel('ChannelsGroupsAlbum');
                foreach($this->request->data['Group']['channel'] as $channel){
                    $data['ChannelsGroupsAlbum']['Group_Id'] = $lastGroupId;
                    $data['ChannelsGroupsAlbum']['Channel_Id'] = $channel;
                    $this->ChannelsGroupsAlbum->save($data);
                }
                $this->Session->setFlash(__('Group has been saved.'));
                $this->redirect(array('action' => 'index'));	   
			}	
		}
        
        public function delete($id = null){
            $this->loadModel('ChannelsGroupsAlbum');
            $this->loadModel('Group');
            $data = $this->request->query;
            $result = $this->ChannelsGroupsAlbum->query("DELETE FROM channels_groups_albums WHERE Channel_Id = '".$data['cheannelId']."' AND Group_Id = '".$data['id']."'");
            if($result){
                $result2 = $this->Group->query("DELETE FROM groups WHERE Id = '".$data['id']."'");
            }
            $this->redirect(array('action' => 'index'));
        }
		
        private function updateGroupName($groupdId, $groupName){
            $this->Group->query("UPDATE groups SET Name = '".$groupName."' WHERE Id = ".$groupdId."");
        }
		
	}
	
?>