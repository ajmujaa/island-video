<?php

App::uses('AppController', 'Controller');

class AdministratorAppController extends AppController {
	
	public $components = array('Auth', 'Cookie');
	
	public function beforeFilter() {
		parent::beforeFilter();
		//$this->Auth->allow();
		if ($this->Session->check('Auth.User')){					
			$userRoleId = $this->Session->read('Users.RollId');
			if((int)$userRoleId === 2){						
				$this->Auth->allow();
			}else{						
				$this->redirect('/');
			}
		}else{					
			$this->redirect('/');					
		}				
    }
	
	public function index(){
		$this->set('title_for_layout', 'Administrator');			
	}

}
