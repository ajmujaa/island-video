<?php
App::uses('AdministratorAppController', 'Administrator.Controller');
/**
 * Categories Controller
 *
 */
class ChannelsController extends AdministratorAppController {

/**
 * Scaffold
 *
 * @var mixed
 */
	public $scaffold;
	public $helpers = array('Html', 'Form');
	public $components = array('Session', 'Category');
	
	public function index(){
		$this->set('title_for_layout', 'Administrator | Channels');				
		$channels = $this->Category->getAllChannels();
		$this->set('channels', $channels);
	}
	
	public function view($id=null){
		$this->set('title_for_layout', 'Administrator | View Channel');				
		if(!$id){
			throw new NotFoundException(__('Invalid Channel'));	
		}
		$channel = $this->Category->getRequestedItem('Channel', $id);
		if(!$channel){
			throw new NotFoundException(__('Invalid Channel'));
		}			
		$this->set('channel', $channel);
	}
	
	public function edit($id = null){
		$this->set('title_for_layout', 'Administrator | Edit Channel');				
		if(!$id){
			throw new NotFoundException(__('Invalid Channel'));	
		}
		$channel = $this->Category->getRequestedItem('Channel', $id);
		if($this->request->is('post') || $this->request->is('put')){	
			$this->Channel->id = $id;
			if($this->Channel->save($this->request->data)){
				$this->Session->setFlash(__('Channel has been Updated.'));
				$this->redirect(array('action' => 'index', '?' => array('type' => $this->request->data['Channel']['Name'])));	
			}else {
                $this->Session->setFlash(__('Unable to add Channel.'));
            }
		}	
		if(!$this->request->data){
			$this->request->data = $channel;	
		}
		
	}
    
    public function delete($id = null){
            $this->loadModel('ChannelsGroupsAlbum');
            $this->loadModel('Channel');
            $result = $this->ChannelsGroupsAlbum->query("DELETE FROM channels_groups_albums WHERE Channel_Id = '".$id."'");
            if($result){
                $result2 = $this->Channel->query("DELETE FROM channels WHERE Id = '".$id."'");
            }
            $this->redirect(array('action' => 'index'));
        }
	
	public function add(){
		if($this->request->is('post')){
			$this->Channel->create();
			if($this->Channel->save($this->request->data)){
				$this->Session->setFlash(__('Channel has been saved.'));
				$this->redirect(array('action' => 'index'));	
			}else {
                $this->Session->setFlash(__('Unable to add Channel.'));
            }
		}	
	}
	

}
