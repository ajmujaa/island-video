<?php 
	App::uses('AdministratorAppController', 'Administrator.Controller');
	
	class AlbumsController extends AdministratorAppController{
		
		public $scaffold;
		public $helpers = array('Html', 'Form');
		public $components = array('Session', 'Category');
		
		public function index(){
            $this->loadModel('ChannelsGroupsAlbum');
			$this->set('title_for_layout', 'Administrator | Albums');	
			$albums = $this->Category->getAllChannelsGroupsForAlbums();	
			$channelName = '';
			$albumName = '';
			$channelsAlbum = array();
			foreach($albums as $album){
                if($album['ChannelsGroupsAlbum']['Album_Id'] !== '0'){
                    $channelName = $this->Category->getChannelName($album['ChannelsGroupsAlbum']['Channel_Id']);
                    $groupName = $this->Category->getGroupName($album['ChannelsGroupsAlbum']['Group_Id']);
                    $albumName = $this->Category->getAlbumName($album['ChannelsGroupsAlbum']['Album_Id']);
				    $channelsAlbum[] = array($album['ChannelsGroupsAlbum']['Album_Id'], $album['ChannelsGroupsAlbum']['Channel_Id'], $albumName, $channelName, $album['ChannelsGroupsAlbum']['Group_Id'], $groupName);
                }
			}     
			$this->set('albums', $this->Category->object_array_unique($channelsAlbum));
		}
		
		public function view($id=null){
			$this->set('title_for_layout', 'Administrator | View Album');				
			if(!$id){
				throw new NotFoundException(__('Invalid Album'));	
			}
			$album = $this->Category->getRequestedItem('Album', $id);
			if(!$album){
				throw new NotFoundException(__('Invalid Album'));
			}			
			$this->set('album', $album);
		}
		
		public function edit($id = null){
            $this->loadModel('ChannelsGroupsAlbum');
			$this->set('title_for_layout', 'Administrator | Edit Album');	
			$data = $this->request->query;			
			if(!$data['id']){
				throw new NotFoundException(__('Invalid Channel'));	
			}
			$this->set('channelId', $data['channelId']);
            $this->set('groupId', $data['groupId']);
			$channel = $this->Category->getRequestedItem('Album', $data['id']);
			$this->set('groups', $this->Category->getRequestedList('Group', 'Name'));
			$this->set('channels', $this->Category->getRequestedList('Channel', 'Name'));
			if($this->request->is('post') || $this->request->is('put')){
                $rdata = $this->request->data['Album'];
                $this->updateAlbumName($rdata['Id'], $rdata['Name']);
                $this->ChannelsGroupsAlbum->query("UPDATE channels_groups_albums SET Group_Id = ".$rdata['GroupId'].", Channel_Id = ".$rdata['ChannelId']." WHERE Album_Id = " . $data['id'] . " AND Channel_Id = ".$data['channelId']." AND  Group_Id = ".$data['groupId']."");
                $this->Session->setFlash(__('Album has been Updated.'));
				$this->redirect(array('action' => 'index', '?' => array('type' => $this->request->data['Album']['Name'])));	
                
			}	
			if(!$this->request->data){
				$this->request->data = $channel;	
			}
			
		}
		
		public function add(){
			$this->set('groups', $this->Category->getRequestedList('Group', 'Name'));
			$this->set('channels', $this->Category->getRequestedList('Channel', 'Name'));
			if($this->request->is('post')){
				$this->Album->create();
				$this->Album->save($this->request->data);
				$lastAlbumId = $this->Album->getLastInsertId();				
				$selectedChannelCount = count($this->request->data['Album']['channel']);
				$selectedGroupCount = count($this->request->data['Album']['group']);
				if($selectedChannelCount > $selectedGroupCount){
					foreach($this->request->data['Album']['group'] as $group){
						foreach($this->request->data['Album']['channel'] as $channel){
							//$this->loadModel('GroupsAlbum');
							$this->addAlbumGroupAndChannels($lastAlbumId, $group, $channel);		
						}
					}
				}else{
					foreach($this->request->data['Album']['channel'] as $channel){
						foreach($this->request->data['Album']['group'] as $group){
							//$this->loadModel('GroupsAlbum');
							$this->addAlbumGroupAndChannels($lastAlbumId, $group, $channel);		
						}
					}					
				}
				$this->Session->setFlash(__('Album has been saved.'));
				$this->redirect(array('action' => 'index'));			
			}	
		}
        
        public function delete($id = null){
            $this->loadModel('ChannelsGroupsAlbum');
            $this->loadModel('Album');
            $data = $this->request->query;
            $result = $this->ChannelsGroupsAlbum->query("DELETE FROM channels_groups_albums WHERE Channel_Id = '".$data['channelId']."' AND Group_Id = '".$data['groupId']."' AND Album_Id = '".$data['id']."'");
            if($result){
                $result2 = $this->Album->query("DELETE FROM albums WHERE Id = '".$data['id']."'");
            }
            $this->redirect(array('action' => 'index'));
        }
		
		private function addAlbumGroupAndChannels($albumId, $groupId, $channelId){
			$this->loadModel('ChannelsGroupsAlbum');
			$data['ChannelsGroupsAlbum']['Album_Id'] = $albumId;
			$data['ChannelsGroupsAlbum']['Channel_Id'] = $channelId;
			$data['ChannelsGroupsAlbum']['Group_Id'] = $groupId;
			if($this->ChannelsGroupsAlbum->save($data)){
				return true;
			}
		}
        
        private function updateAlbumName($albumId, $albumName){
            $this->Album->query("UPDATE albums SET Name = '".$albumName."' WHERE Id = ".$albumId."");   
        }
		
	}
	
?>