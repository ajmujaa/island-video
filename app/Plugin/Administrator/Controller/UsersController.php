<?php
App::uses('AdministratorAppController', 'Administrator.Controller');
/**
 * Users Controller
 *
 */
class UsersController extends AdministratorAppController {

/**
 * Scaffold
 *
 * @var mixed
 */
	public $scaffold;
	public $helpers = array('Html', 'Form');
	public $components = array('Session', 'VimeoAuth', 'VimeoVideo');
	
	public function index(){
		$this->set('title_for_layout', 'Administrator | Users');			
		$usertype = $this->request->query['type'];
		$users = $this->User->find('all', array('conditions' => array('User.ActivationtypeId = ' => '1', 'User.RoleId = ' => $usertype)));	
		$this->set('users', $users);	
	}
	
	public function add(){
		if($this->request->is('post')){
			//$imgname = $this->upload($this->request->data['User']['Photo']['name'], './img/profiles/');
			$this->request->data['User']['ActivationtypeId'] = '1';
			$this->request->data['User']['CreatedDate'] = date("Y-m-d H:i:s");
			$this->request->data['User']['UpdatedDate'] = date("Y-m-d H:i:s");
			$this->request->data['User']['RegisterIP'] = $_SERVER['REMOTE_ADDR'];
			$this->request->data['User']['FullName'] = $this->request->data['User']['FirstName'] . ' ' . $this->request->data['User']['LastName'];
			$this->request->data['User']['Photo'] = "";
			$this->User->create();
			if($this->User->save($this->request->data)){
				$this->Session->setFlash(__('User has been saved.'));
				$this->redirect(array('action' => 'index', '?' => array('type' => $this->request->data['User']['RoleId'])));	
			}else {
                $this->Session->setFlash(__('Unable to add User.'));
            }
		}
	}
	
	public function view($id = null){
		$vimeo = $this->VimeoAuth->doAuth();
		if(!$id){
			throw new NotFoundException(__('Invalid User'));	
		}
		$user = $this->User->findById($id);
		if(!$user){
			throw new NotFoundException(__('Invalid User'));
		}
		$this->set('user', $user);
		$this->set('videos', $this->VimeoVideo->getVideosByUserId($id, $vimeo));		
	}
	
	public function edit($id = null){
		if(!$id){
			throw new NotFoundException(__('Invalid User'));	
		}
		$user = $this->User->findById($id);
		if(!$user){
			throw new NotFoundException(__('Invalid User'));
		}
		if($this->request->is('post') || $this->request->is('put')){	
			//$imgname = $this->upload($this->request->data['User']['Photo']['name'], './img/profiles/');
			$this->User->id = $id;
			$this->request->data['User']['FullName'] = $this->request->data['User']['FirstName'] . ' ' . $this->request->data['User']['LastName'];
			$this->request->data['User']['Photo'] = "";
			$this->request->data['User']['UpdatedDate'] = date("Y-m-d H:i:s");
			if($this->User->save($this->request->data)){
				$this->Session->setFlash(__('User has been Updated.'));
				$this->redirect(array('action' => 'index', '?' => array('type' => $this->request->data['User']['RoleId'])));	
			}else {
                $this->Session->setFlash(__('Unable to add User.'));
            }
		}	
		if(!$this->request->data){
			$this->request->data = $user;	
		}
	}
	
	private function upload($file, $uploadPath){
		$filename = $file;
		$ext = explode(".", $filename);
		$imgname = md5($filename).".".$ext[1];
		move_uploaded_file($this->request->data['User']['Photo']['tmp_name'],
$uploadPath . basename($imgname));	
		return $imgname;
	}

}
