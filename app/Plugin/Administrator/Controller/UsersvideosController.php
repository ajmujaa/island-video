<?php 

	App::uses('AdministratorAppController', 'Administrator.Controller');
		
	class UsersvideosController extends AdministratorAppController{
		
		public $components = array('VimeoAuth', 'VimeoVideo', 'User');
		
		public function index(){
			$this->set('title_for_layout', 'Administrator | Assign Videos to Users');
		}
		
		public function add($userId = null){
			$vimeoAuth = $this->VimeoAuth->doAuth();
			$username = $this->VimeoAuth->getUsername();
			$this->set('videos', $this->VimeoVideo->getAllVimeoVideo($username, $vimeoAuth, $userId));
			$this->set('user', $this->User->getSingleUser($userId));
			$this->Usersvideo->create();
			if($this->request->is('post')){
				$data = array();
				foreach($this->request->data['Usersvideo']['VideosId'] as $video){
					$this->request->data['Usersvideo']['UsersId'] = $userId;
					$this->request->data['Usersvideo']['VideosId'] = $video;
					$data[] = $this->request->data;
				}
				
				if($this->Usersvideo->saveMany($data)){
					$this->redirect(array('controller' => 'Users', 'action' => 'index', '?' => array('type' => '1')));	
				}
			}				
		}
					
	}

?>