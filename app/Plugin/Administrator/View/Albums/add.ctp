<h1>Add Album</h1>
<?php
echo $this->Form->create('Album');
echo $this->Form->input('Name', array('type' => 'text'));
echo $this->Form->label('ChannelId', 'Channel');
	while (list($key, $val) = each($channels)) {
		echo $this->Form->input($val, array('type' => 'checkbox', 'value' => $key, 'name' => 'data[Album][channel][]', 'hiddenField' => false));		
	}
	
echo $this->Form->label('GroupId', 'Group');
	while (list($key, $val) = each($groups)) { 
		echo $this->Form->input($val, array('id' => $key, 'type' => 'checkbox', 'value' => $key, 'name' => 'data[Album][group][]', 'hiddenField' => false, 'label' => array('for' => $key)));		
	}
	
echo $this->Form->end('Save Album');
?>