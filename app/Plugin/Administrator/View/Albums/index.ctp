<h3>Album</h3>
<?php 
	echo $this->Html->link('Add Album', array('action' => 'add'), array('class' => 'link'));
?><br/><br/>
<table class="datatable" id="datatable">
	<thead>
        <tr>
            <th>Id</th>
            <th>Album</th>
            <th>Channel</th>
            <th>Group</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($albums as $album):?>
    <tr>
        <td><?php echo $album[0]; ?></td>
        <td><?php echo $this->Html->link($album[2],
array('controller' => 'albums', 'action' => 'view', $album[0])); ?></td>     
		<td><?php echo $this->Html->link($album[3],
array('controller' => 'channels', 'action' => 'view', $album[1])); ?></td>     
		<td><?php echo $this->Html->link($album[5],
array('controller' => 'groups', 'action' => 'view', $album[4])); ?></td>
        <td class="actions">
        	<?php echo $this->Html->link('View', array('action' => 'view', $album[0]), array('class' => 'link')); ?>		
            <?php echo $this->Html->link('Edit', array('action' => 'edit', '?' => array('id' => $album[0], 'channelId' => $album[1], 'groupId' => $album[4])), array('class' => 'link')); ?>
            <?php echo $this->Html->link('Delete', array('action' => 'delete', '?' => array('id' => $album[0], 'channelId' => $album[1], 'groupId' => $album[4])), array('class' => 'link')); ?>
        </td>
    </tr>
    <?php endforeach; ?>
    <?php unset($album); ?>
    </tbody>
</table>