<!-- File: /app/View/Video/view.ctp --> 
<h3><?php //echo $channel['Channel']; ?></h3>
<?php 
	echo $this->Form->create('Album');
	echo $this->Form->input('Name');
	echo $this->Form->input('ChannelId', array('type'=>'select', 'label'=>'Channel', 'options'=>$channels, 'selected'=>$channelId));
    echo $this->Form->input('GroupId', array('type'=>'select', 'label'=>'Group', 'options'=>$groups, 'selected'=>$groupId));
	echo $this->Form->input('Id', array('type' => 'hidden'));
	echo $this->Form->end('Update Album');
?>