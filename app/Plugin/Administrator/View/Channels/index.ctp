<h3>Channels</h3>
<?php 
	echo $this->Html->link('Add Channel', array('action' => 'add'), array('class' => 'link'));
?><br/><br/>
<table class="datatable" id="datatable">
	<thead>
        <tr>
            <th>Id</th>
            <th>Channel</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($channels as $channel): ?>
    <tr>
        <td><?php echo $channel['Channel']['Id']; ?></td>
        <td><?php echo $this->Html->link($channel['Channel']['Name'],
array('controller' => 'channels', 'action' => 'view', $channel['Channel']['Id'])); ?></td>     
        <td class="actions">
        	<?php echo $this->Html->link('View', array('action' => 'view', $channel['Channel']['Id']), array('class' => 'link')); ?>
            <?php echo $this->Html->link('Edit', array('action' => 'edit', $channel['Channel']['Id']), array('class' => 'link')); ?>
            <?php echo $this->Html->link('Delete', array('action' => 'delete', $channel['Channel']['Id']), array('class' => 'link')); ?>
        </td>
    </tr>
    <?php endforeach; ?>
    <?php unset($channel); ?>
    </tbody>
</table>