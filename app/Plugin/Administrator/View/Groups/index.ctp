<h3>Groups</h3>
<?php 
	echo $this->Html->link('Add Group', array('action' => 'add'), array('class' => 'link'));
?><br/><br/>
<table class="datatable" id="datatable">
	<thead>
        <tr>
            <th>Id</th>
            <th>Group</th>
            <th>Channel</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($groups as $group): ?>
    <tr>
        <td><?php echo $group[0]; ?></td>
		<td><?php echo $this->Html->link($group[2],
array('controller' => 'groups', 'action' => 'view', $group[0])); ?></td>     
		<td><?php echo $this->Html->link($group[3],
array('controller' => 'channels', 'action' => 'view', $group[1])); ?></td>     
        <td class="actions">
        	<?php echo $this->Html->link('View', array('action' => 'view', $group[0]), array('class' => 'link')); ?>
            <?php echo $this->Html->link('Edit', array('action' => 'edit', '?' => array('id' => $group[0], 'channelId' => $group[1])), array('class' => 'link')); ?>
            <?php echo $this->Html->link('Delete', array('action' => 'delete', '?' => array('id' => $group[0], 'cheannelId' => $group[1])), array('class' => 'link')); ?>
        </td>
    </tr>
    <?php endforeach; ?>
    <?php unset($group); ?>
    </tbody>
</table>