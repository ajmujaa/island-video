<nav class="side-menu col-xs-2">
	<ul>
		<li><?php echo $this->Html->link('Dashboard', array('controller' => 'administrator', 'action' => 'index')); ?></li>
		<li><?php echo $this->Html->link('Users', array('controller' => 'users', 'action' => 'index', '?' => array('type' => '1'))); ?></li>
		<li><?php echo $this->Html->link('Videos', array('controller' => 'videos', 'action' => 'index')); ?></li>
		<li><?php echo $this->Html->link('Channels', array('controller' => 'channels', 'action' => 'index')); ?></li>
        <li><?php echo $this->Html->link('Groups', array('controller' => 'groups', 'action' => 'index')); ?></li>
        <li><?php echo $this->Html->link('Albums', array('controller' => 'albums', 'action' => 'index')); ?></li>
		<li><?php echo $this->Html->link('Lecturers', array('controller' => 'users', 'action' => 'index', '?' => array('type' => '3'))); ?></li>
	</ul>
</nav>