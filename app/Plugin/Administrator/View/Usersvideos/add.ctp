<h3>Assign Videos</h3>
<div class="mini-profile col-xs-12">
	<div class="profile-img col-xs-2"><?php echo $this->Html->image("default_profile_large.jpg", array('fullBase' => true, 'width' => '150')); ?></div>
	<div class="profile-name col-xs-10">
		<p><?php echo $user['User']['FullName']; ?></p>
        <p><?php echo 'Remaining Tokens : ' . $user['User']['RemainingTokens']; ?></p>
	</div>
</div>
<div>
	<?php 
		echo $this->Form->create('Usersvideo');
	?>
    <table class="datatable" id="datatable">
        <thead>
            <tr>
                <th>Video Id</th>
                <th>Title</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        	<?php foreach($videos as $video): ?>	
            <tr>
            	<td><?php echo $video->id; ?></td>
                <td><?php echo $video->title; ?></td>
                <td><?php echo $this->Form->input('VideosId.', array('type' => 'checkbox', 'value' => $video->id, 'hiddenField' => false, 'class' => 'assign')); ?></td>
            </tr>
			<?php endforeach; ?>
        </tbody>
    </table>
	<?php       
		
		echo $this->Form->end('Assign Videos');
	?>
</div>