<!-- File: /app/View/Users/index.ctp -->
<?php echo $this->Html->link($this->request->query['type'] == '1' ? 'Add User' : 'Add Lecturer', array('controller' => 'users', 'action' => 'add', '?' => array('type' => $this->request->query['type'])), array('class' => 'link')); ?>
<table class="table">
    <tr>
        <th>Id</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Email</th>
		<th>Action</th>
    </tr>
    <?php foreach ($users as $user): ?>
    <tr>
        <td><?php echo $user['User']['Id']; ?></td>
        <td><?php echo $this->Html->link($user['User']['FirstName'],
array('controller' => 'users', 'action' => 'view', $user['User']['Id'])); ?></td>     
        <td><?php echo $user['User']['LastName']; ?></td>
        <td><?php echo $user['User']['email']; ?></td>
		<td class="actions">
			<?php echo $this->Html->link('View', array('action' => 'view', $user['User']['Id']), array('class' => 'link')); ?>
			<?php echo $this->Html->link('Edit', array('action' => 'edit', $user['User']['Id']), array('class' => 'link')); ?>
		</td>
    </tr>
    <?php endforeach; ?>
    <?php unset($user); ?>
</table>
