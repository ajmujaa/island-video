<h1>Update User</h1>
<?php
echo $this->Form->create('User', array(
    'enctype' => 'multipart/form-data'
));
echo $this->Form->input('FirstName');
echo $this->Form->input('LastName');
echo $this->Form->input('Photo', array('type' => 'file'));
echo $this->Form->input('email');
echo '<div class="input text">';
echo $this->Form->label('Address', 'Address');
echo $this->Form->textarea('Address');
echo '</div>';
echo $this->Form->input('RemainingTokens', array('label' => 'Tokens'));
echo '<div class="input text">';
echo $this->Form->label('RoleId', 'Role');
echo $this->Form->select('RoleId', array('1' => 'User', '2' => 'Admin','3' => 'Lecturer'), array('empty' => 'Select Role'));
echo '</div>';
echo $this->Form->input('Id', array('type' => 'hidden'));
echo $this->Form->end('Update User');
?>