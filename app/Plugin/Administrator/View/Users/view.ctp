<!-- File: /app/View/Users/view.ctp --> 
<div class="item-view">
<p><label>First Name</label>:<?php echo h($user['User']['FirstName']); ?></p>
<p><label>Last Name</label>:<?php echo $user['User']['LastName'];?></p>
<p><label>Email</label>:<?php echo h($user['User']['email']); ?></p>
<p><label>Address</label>:<?php echo h($user['User']['Address']); ?></p>
<p><label>Role</label>:<?php echo h($user['Userole']['Name']); ?> </p>
<p><label>Remaining Points</label>:<?php echo $user['User']['RemainingTokens']; ?></p>
<p><label>Videos</label></p>
	<div class="large-block-grid-4">		  
		  	<?php 
				foreach($videos as $video):
			?>
			<div class="item">
            	<?php echo $video[0]->type == "Premium" ? '<div class="ribbon"></div>' : ''; ?>
			  <div class="thumb"> 
              <?php 
			  	echo $this->Html->image($video[0]->thumbnails->thumbnail[1]->_content, array(
					"alt" => $video[0]->title,
					'url' => array('controller' => 'Videos', 'action' => 'view', $video[0]->id)));
				echo '<a href="/islandvideo/Videos/view/'.$video[0]->id.'" class="view"><span class="vertical-align"></span> </span> <span class="overlay"></span></a>';
				echo '<span class="duration">'.gmdate("H:i:s", $video[0]->duration).'</span>';
			  ?>
              </div>
			  <div class="data">
				<h6 class="entry-title font_bold font_bold">
                	<?php echo $this->Html->link($this->Text->truncate($video[0]->title, 50, array('ellipsis' => '...', 'exact' => false)),
array('controller' => 'videos', 'action' => 'view', $video[0]->id), array('title' => $video[0]->title)); ?> </h6>
			  </div>
			</div>
			<?php endforeach; ?>
			
		  </div>
<p><label></label><?php echo $this->Html->link('Edit User', array('action' => 'edit', $user['User']['Id']), array('class' => 'link')); ?></p>
