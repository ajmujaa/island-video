<h1>Add User</h1>
<?php
echo $this->Form->create('User', array(
    'enctype' => 'multipart/form-data'
));
echo $this->Form->input('FirstName');
echo $this->Form->input('LastName');
echo $this->Form->input('Photo', array('type' => 'file'));
echo $this->Form->input('email');
echo '<div class="input text">';
echo $this->Form->label('Address', 'Address');
echo $this->Form->textarea('Address');
echo '</div>';
echo $this->Form->input('RemainingTokens', array('label' => 'Tokens'));
echo $this->Form->input('RoleId', array('type' => 'hidden', 'value' => $this->request->query['type']));
echo $this->Form->end('Save User');
?>