<h3>Videos</h3>
<?php 
	echo $this->Form->create('Video', array('action' => 'addAll'));
	echo $this->Form->end('Add Videos');
?>
<table class="datatable" id="datatable">
	<thead>
        <tr>
            <th>Id</th>
            <th>Video</th>
            <th>Title</th>
            <th>Description</th>
            <th>Duration</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($videos as $video): ?>
    <tr>
        <td><?php echo $video->id; ?></td>
        <td><!--<iframe src="//player.vimeo.com/video/<?php echo $video->id; ?>" width="200" height="150" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>--></td>
        <td><?php echo $this->Html->link($video->title,
array('controller' => 'videos', 'action' => 'view', $video->id)); ?></td>     
        <td width="200"><?php echo $video->description; ?></td>
        <td><?php echo $video->duration; ?> Seconds</td>        
        <td class="actions">
        	<?php echo $this->Html->link('View', array('action' => 'view', $video->id), array('class' => 'link')); ?>		<?php echo $this->Html->link('Edit', array('action' => 'edit', $video->id), array('class' => 'link')); ?>
        </td>
    </tr>
    <?php endforeach; ?>
    <?php unset($video); ?>
    </tbody>
</table>