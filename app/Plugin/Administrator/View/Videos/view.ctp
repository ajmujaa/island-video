<!-- File: /app/View/Video/view.ctp --> 
<h3><?php echo $video[0]->title; ?></h3>
<div class="player">
<iframe src="//player.vimeo.com/video/<?php echo $video[0]->id; ?>?title=0&portrait=0&byline=0&badge=0" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
</div>
<div class="video-details">
<p class="video-desp"><?php echo $video[0]->description; ?></p>
<p><label>Duration</label> : <?php echo $video[0]->duration; ?> Seconds</p>
<p><label>Uploaded Date</label> : <?php echo $video[0]->upload_date; ?></p>
<p><label>No. of times played</label> : <?php echo $video[0]->number_of_plays; ?></p>
<p><label>Tags</label> : <?php 
							$output = array();
							foreach($video[0]->tags->tag as $tag):
								$output[] = $tag->normalized;
							endforeach;
							echo implode(', ', $output); 
						?>
</p>
<p><label>Token Value</label> : <?php echo $videodata['Video']['TokenValue']; ?></p>
<p><label>Total Revenu</label> : <?php echo $videodata['Video']['TotalRevenue']; ?></p>
<p><label>Author</label> : <?php echo $videodata['User']['FirstName'].' '.$videodata['User']['LastName']; ?></p>
<p><label></label><?php echo $this->Html->link('Edit Video', array('action' => 'edit', $video[0]->id), array('class' => 'link')); ?></p>
</div>
