<!-- File: /app/View/Video/view.ctp --> 
<h3><?php echo $video[0]->title; ?></h3>
<div class="player">
<iframe src="//player.vimeo.com/video/<?php echo $video[0]->id; ?>?title=0&portrait=0&byline=0&badge=0" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
</div>
<div class="video-details">
<p class="video-desp"><?php echo $video[0]->description; ?></p>
<p><label>Duration</label> : <?php echo $video[0]->duration; ?> Seconds</p>
<p><label>Uploaded Date</label> : <?php echo $video[0]->upload_date; ?></p>
<p><label>No. of times played</label> : <?php echo $video[0]->number_of_plays; ?></p>
<p><label>Tags</label> : <?php 
							$output = array();
							foreach($video[0]->tags->tag as $tag):
								$output[] = $tag->normalized;
							endforeach;
							echo implode(', ', $output); 
						?>
</p>

<?php 
	echo $this->Form->create('Video');
	echo $this->Form->input('TokenValue');
	echo '<div class="input text">';
	echo $this->Form->label('AuthorId', 'Author');
	echo $this->Form->select('AuthorId', $leturers);
	echo '</div>';
	echo '<div class="input text">';
	echo $this->Form->label('VideotypeId', 'Video Type');
	echo $this->Form->select('VideotypeId', $videoTypes);
	echo '</div>';
	echo '<div class="input text">';
	echo $this->Form->label('ChannelId', 'Channel');
	echo $this->Form->select('ChannelId', $channels);
	echo '</div>';
	echo '<div class="input text">';
	echo $this->Form->label('GroupId', 'Group');
	while (list($key, $val) = each($groups)) {
		echo $this->Form->input($val, array('type' => 'checkbox', 'value' => $key, 'class' => 'assign', 'hiddenField' => false, 'name' => 'data[Video][group][]', in_array($key, $group) ? 'checked' : ''));	
		
	}
	echo '</div>';
	echo '<div class="input text">';
	echo $this->Form->label('AlbumId', 'Album');
	while (list($key, $val) = each($albums)) {
		echo $this->Form->input($val, array('type' => 'checkbox', 'value' => $key, 'class' => 'assign', 'hiddenField' => false, 'name' => 'data[Video][album][]', in_array($key, $album) ? 'checked' : ''));			
	}
	echo '</div>';
	echo $this->Form->input('Id', array('type' => 'hidden'));
	echo $this->Form->end('Update Video');
?>
</div>
