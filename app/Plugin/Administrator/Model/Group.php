<?php 

	class Group extends AppModel{
		
		public $primaryKey = 'Id';
		
		//public $belongsTo = array('Channel' => array('className' => 'Channel', 'foreignKey' => 'ChannelId'));
		public $hasAndBelongsToMany = array(
			'Channel' =>
				array(
					'className' => 'Group',
					'joinTable' => 'channels_groups',
					'foreignKey' => 'Channel_Id',
					'associationForeignKey' => 'Group_Id',
					'unique' => false
				)
			);
		
	}

?>