<?php 
	
	class Video extends AppModel{
		
		public $belongsTo = array(
			'User' => array('className' => 'User', 'foreignKey' => 'AuthorId'), 
			'Videotype' => array('className' => 'Videotype', 'foreignKey' => 'VideotypeId')
		);	

	}
	
?>