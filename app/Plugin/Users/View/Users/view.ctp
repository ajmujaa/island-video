<?php
/**
 * Copyright 2010 - 2013, Cake Development Corporation (http://cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010 - 2013, Cake Development Corporation (http://cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<?php /*?><div class="users view">
<h2><?php echo __d('users', 'User Details'); ?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		
		<a><?php echo __d('users', 'Username'); ?> - 
			<?php echo $user[$model]['username'];  ?>
		</a> </br>	
		<a><?php echo __d('users', 'FullName'); ?> - 
			<?php echo $user[$model]['FullName'];  ?>
		</a> </br>
		<a><?php echo __d('users', 'email'); ?> - 
			<?php echo $user[$model]['email'];  ?>
		</a> </br>
		<a><?php echo __d('users', 'Address'); ?> - 
			<?php echo $user[$model]['Address'];  ?>
		</a> </br>
		<a><?php echo __d('users', 'EarnedTokens'); ?> - 
			<?php echo $user[$model]['EarnedTokens'];  ?>
		</a> </br>
		<a><?php echo __d('users', 'RemainingTokens'); ?> - 
			<?php echo $user[$model]['RemainingTokens'];  ?>
		</a> </br>
		
		
		
		
		
		<?php
		if (!empty($user['UserDetail'])) {
			foreach ($user['UserDetail'] as $section => $details) {
				foreach ($details as $field => $value) {
					echo '<dt>' . $section . ' - ' . $field . '</dt>';
					echo '<dd>' . $value . '</dd>';
				}
			}
		}
		?>
	</dl>
</div><?php */?>
<?php //echo $this->element('Users.Users/sidebar'); ?>

<?php
/**
 * Copyright 2010 - 2013, Cake Development Corporation (http://cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010 - 2013, Cake Development Corporation (http://cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<!--<div class="users view">
<h2><?php echo __d('users', 'User'); ?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __d('users', 'Username'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $user[$model]['username']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __d('users', 'Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $user[$model]['CreatedDate']; ?>
			&nbsp;
		</dd>
		<?php
		if (!empty($user['UserDetail'])) {
			foreach ($user['UserDetail'] as $section => $details) {
				foreach ($details as $field => $value) {
					echo '<dt>' . $section . ' - ' . $field . '</dt>';
					echo '<dd>' . $value . '</dd>';
				}
			}
		}
		?>
	</dl>
</div>
<?php echo $this->element('Users.Users/sidebar'); ?>-->

<div class="row margin-top10px profile-main-theme">

<div class="large-16 columns">   
    <!-- Begin Item content display -->    

      <section class="section profile-section">
         <div class="large-16 columns profile-column">  
		 
				<div class="large-5 columns profile-column">  
					<div class="left user-image">
						<img src="<?php echo 'http://'.$_SERVER['HTTP_HOST'].'/islandvideo/'; ?>img/default_profile_large.jpg"/>
						<div class="image-holder-name">
							<p><?php echo $user[$model]['FullName'];  ?></p>
						</div>
					</div>					
				</div>
				
				<div class="large-11 columns profile-column">  
					<h3 class="text-center border-bottom">Personel Information</h3>
					
					<div class="large-16 columns profile-column">  
						<div class="right profile-user-details">
							<div class="profile-option-title left width30"><?php echo __d('users', 'Username'); ?> : </div>
							<div class="profile-option-data width70"><?php echo $user[$model]['username'];  ?></div>
							<div class="profile-option-title left width30"><?php echo __d('users', 'email'); ?> : </div>
							<div class="profile-option-data width70"><?php echo $user[$model]['email'];  ?></div>
                            <div class="profile-option-title left width30"><?php echo __d('users', 'Address'); ?> : </div>
							<div class="profile-option-data width70"><?php echo $user[$model]['Address'];  ?></div>
							<div class="profile-option-title left width30"><?php echo __d('users', 'EarnedTokens'); ?> : </div>
							<div class="profile-option-data width70"><?php echo $user[$model]['EarnedTokens'];  ?></div>
							<div class="profile-option-title left width30"><?php echo __d('users', 'RemainingTokens'); ?> : </div>
							<div class="profile-option-data width70"><?php echo $user[$model]['RemainingTokens'];  ?></div>
							
						</div>			
					</div>
					
					<div class="right">
						<!--<a class="edit-details">Edit Details</a>-->					
					</div>
					
				</div>
		 </div>
		 
		<div class="large-16 columns  bg-yellow-green">  
			<div class="profile-bottom-content"> 				
				<div class="large-5 columns profile-column">
					<dl class="tabs vertical" data-tab> 
<!--						<dd class="active"><a href="#panel1a"><i class="fa fa-home"></i>	Tab 1</a></dd> -->
						<dd class="active"><a href="#panel2a"><i class="fa fa-random"></i>Video History</a></dd> 
<!--
						<dd><a href="#panel3a"><i class="fa fa-cogs"></i>	Tab 3</a></dd> 
						<dd><a href="#panel4a"><i class="fa fa-cogs"></i>	Tab 4</a></dd> 
						<dd><a href="#panel5a"><i class="fa fa-cogs"></i>	Tab 5</a></dd> 
-->
					</dl> 
				</div>
				
				<div class="large-11 columns">
					<div class="tabs-content"> 
<!--
						<div class="content active" id="panel1a">
								<div id="wrapping" class="clearfix css-forms">
									<section id="aligned">
										<input type="text" name="name" id="name" placeholder="Your name" autocomplete="off" tabindex="1" class="txtinput">
										<input type="email" name="email" id="email" placeholder="Your e-mail address" autocomplete="off" tabindex="2" class="txtinput">
										<input type="tel" name="telephone" id="telephone" placeholder="Phone number" tabindex="4" class="txtinput">
										<select id="recipient" name="recipient" tabindex="6" class="selmenu">
											<option value="staff">Student 1</option>
											<option value="staff">Student 1</option>
											<option value="staff">Student 1</option>
											<option value="staff">Student 1</option>
											<option value="staff">Student 1</option>
										</select>
										
										<textarea name="message" id="message" placeholder="Enter a cool message..." tabindex="5" class="txtblock"></textarea>
											
									</section>
								</div>								
						</div> 
-->
						<div class="content active" id="panel2a"> 
                            
                        </div> 
<!--
						<div class="content" id="panel3a"> <p>Panel 3 content goes here.</p> </div> 
						<div class="content" id="panel4a"> <p>Panel 4 content goes here.</p> </div> 
						<div class="content" id="panel5a"> <p>Panel 5 content goes here.</p> </div> 
-->
					</div>
				</div>
				
			</div>
		</div>
		 
      </section>
    <!-- End Item content display -->
    
  </div>
  <!-- End Contact Details -->
  
</div>










