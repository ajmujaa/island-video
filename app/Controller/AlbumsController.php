<?php 
	App::uses('AppController', 'Controller');
	App::import('Vendor', 'Vimeo/phpVimeo');

	class AlbumsController extends AppController{
		
		public $components = array('Auth', 'VimeoAuth', 'VimeoVideo', 'Category');
		public $helpers = array('Cache');
		
		public function beforeFilter() {
       		parent::beforeFilter();
        	$this->Auth->allow();
      	}
		
		public function view($albumId = null){
			$this->set('title_for_layout', 'Island Video | Albums');
			$vimeo = $this->VimeoAuth->doAuth();
			$username = $this->VimeoAuth->getUsername();
            
			$videos = $this->VimeoVideo->getVideosByAlbum($albumId, $vimeo);
            $videoIdArray = array();
            foreach($videos as $video){
                $videoIdArray[] = $video[0]->id;    
            }
            
            $authors = $this->VimeoVideo->getAuthorsbyVideos($videoIdArray);
			$mostViewed = $this->VimeoVideo->getMostViewedVideos($username, 5, $vimeo);
			$this->set('albumName', $this->Category->getAlbumName($albumId));
			$this->set('mostViewed', $mostViewed);
			$this->set('albumId', $albumId);
			$this->set('videos', $videos);
            $this->set('authors', $authors);
		}
				
	}
?>