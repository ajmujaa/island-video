<?php 
	App::uses('AppController', 'Controller');
	App::import('Vendor', 'Vimeo/phpVimeo');

	class GroupsController extends AppController{
		
		public $components = array('Auth', 'VimeoAuth', 'VimeoVideo', 'Category');
		public $helpers = array('Cache');
		
		public function beforeFilter() {
       		parent::beforeFilter();
        	$this->Auth->allow();
      	}
		
		public function view($groupId = null){
            $groupId = $this->request->query['group_id'];
            $channelId = $this->request->query['channel_id'];
            
			$this->set('title_for_layout', 'Island Video | Albums');
			$vimeo = $this->VimeoAuth->doAuth();
			$username = $this->VimeoAuth->getUsername();
            $albums = $this->VimeoVideo->getAlbumsByGroupsAndChannel($groupId, $channelId);
            
            $videos = array();
            foreach($albums as $album){
                $videos[] = $this->VimeoVideo->getVideosByGroupAndAlbum($groupId, $album[0], $vimeo);
            }
            
            $videoIdArray = array();
            foreach($videos as $video){
                foreach($video as $videoId){
                    $videoIdArray[] = $videoId[0]->id;        
                }
            }
            $authors = $this->VimeoVideo->getAuthorsbyVideos($videoIdArray);
            
			$albumVideos = $this->VimeoVideo->getAllVideosByChannel($username, $vimeo);
			$mostViewed = $this->VimeoVideo->getMostViewedVideos($username, 5, $vimeo);
			$this->set('groupName', $this->Category->getGroupName($groupId));
			$this->set('albumVideos', $albumVideos);
			$this->set('mostViewed', $mostViewed);
			$this->set('groupId', $groupId);
			$this->set('albums', $albums);
            $this->set('videos', $videos);
            $this->set('authors', $authors);
		}
				
	}
?>
