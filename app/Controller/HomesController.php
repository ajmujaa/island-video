<?php

	App::uses('AppController', 'Controller');
	App::import('Vendor', 'Vimeo/phpVimeo');

	class HomesController extends AppController {
	
		public $uses = array();
		public $components = array('Auth', 'VimeoAuth', 'VimeoVideo', 'Cookie', 'Category');
		
		public function beforeFilter() {
       		parent::beforeFilter();
        	$this->Auth->allow();
      	}
		
		public function index() {
			$this->set('title_for_layout', 'Island Video | Welcome');				
			$vimeo = $this->VimeoAuth->doAuth();
			$username = $this->VimeoAuth->getUsername();
			
			$channels = $this->Category->getAllChannels();
			$channelVideos = $this->VimeoVideo->getAllVideosByChannel($username, $vimeo);	
            
			$groups = $this->Category->getAllGroups();
			$groupVideos = $this->VimeoVideo->getAllVideosByGroup($username, $vimeo);
            $groupChannels = $this->Category->getAllChannelGroups();
            $filteredGroups = $this->Category->filteredAllGroups();
            //debug($groupChannels);
			
			$mostViewed = $this->VimeoVideo->getMostViewedVideos($username, 5, $vimeo);
			$tags = $this->VimeoVideo->getAllTags($username, $vimeo);
			
			$categories = $this->VimeoVideo->getGroupsForChannel();
			$albums = $this->VimeoVideo->getAlbumsForGroup();
			
			$username = $this->Auth->user('username');
 			$fullName = $this->Auth->user('FullName');
 			$this->Session->write('Users.username', $username);
			$this->Session->write('Users.FullName', $fullName);
			
			$this->set('channels', $channels);	
			$this->set('channelVideos', $channelVideos);
			
			$this->set('groups', $groups);
			$this->set('groupVideos', $groupVideos);
            $this->set('groupChannels', $groupChannels);
            $this->set('filteredGroups', $filteredGroups);
			
			$this->set('categories', $categories);
			$this->set('albums', $albums);
			
			$this->set('tags', $tags);
			$this->set('mostViewed', $mostViewed);			
		}
		
				
	}
