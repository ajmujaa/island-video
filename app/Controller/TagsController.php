<?php 
	App::uses('AppController', 'Controller');
	App::import('Vendor', 'Vimeo/phpVimeo');
	
	class TagsController extends AppController{
		
		public $components = array('VimeoAuth', 'VimeoVideo');
		
		public function view($tag = null){
			$this->set('title_for_layout', 'Island Video | '.$tag);
			$vimeo = $this->VimeoAuth->doAuth();
			$username = $this->VimeoAuth->getUsername();
			$videos = $this->VimeoVideo->getVideosByTag($username, $vimeo, $tag);	
			$mostViewed = $this->VimeoVideo->getMostViewedVideos($username, 5, $vimeo);
			$this->set('tagvideos', $videos);
			$this->set('mostViewed', $mostViewed);
			$this->set('tag', $tag);
		}
			
	}
	
?>