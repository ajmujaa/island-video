<?php 
	App::uses('AppController', 'Controller');
	App::import('Vendor', 'Vimeo/phpVimeo');
	
	class VideosController extends AppController{
		
		public $components = array('Auth', 'VimeoAuth', 'VimeoVideo', 'Category');
		public $helpers = array('Cache');
		
		public function beforeFilter() {
       		parent::beforeFilter();
        	$this->Auth->allow();
      	}
	
		public $cacheAction = array(
			'view' => 48000
		);
		
		public function view($id = null){
			$vimeo = $this->VimeoAuth->doAuth();
			$username = $this->VimeoAuth->getUsername();
			$video = $this->VimeoVideo->getVimeoVideo($id, $vimeo);
			$relatedVideos = $this->VimeoVideo->getRelatedVideosByAlbum($this->getVideoAlbums($id), $vimeo);
			$this->set('title_for_layout', 'Island Video | ' . $video[0]->title);
			$this->set('video', $this->VimeoVideo->object_array_unique($video));	
			$this->set('related', $relatedVideos);
			$this->set('allvideos', $this->VimeoVideo->getAllVideos($username, $vimeo));
			$this->set('similarvideos', $this->VimeoVideo->getSimilarVideosByTag($username, $vimeo, $id));
			$this->set('usertoken', $this->getUserRemainingToken());
		}
		
		public function author($id = null){
			$username = $this->VimeoAuth->getUsername();
			$vimeo = $this->VimeoAuth->doAuth();
			$videos = $this->Video->find('all', array('conditions' => array('AuthorId' => $id)));
			$mostViewed = $this->VimeoVideo->getMostViewedVideos($username, 5, $vimeo);
			$authorVideos = array();
			$author = $videos[0]['User']['FullName'];
			foreach($videos as $video){
				$authorVideos[] = $this->VimeoVideo->getVimeoVideo($video['Video']['Id'], $vimeo);
			}
			$this->set('authorVideos', $authorVideos);
			$this->set('mostViewed', $mostViewed);			
			$this->set('author', $author);
		}
		
		public function album($albumId = null){
			$vimeo = $this->VimeoAuth->doAuth();
			$username = $this->VimeoAuth->getUsername();
			$tags = $this->VimeoVideo->getAllTags($username, $vimeo);									
			$mostViewed = $this->VimeoVideo->getMostViewedVideos($username, 5, $vimeo);			
			$videos = $this->VimeoVideo->getVideosByAlbum($albumId, $vimeo);
			$album = $this->Category->getAlbumName($albumId);
			$this->set('videos', $videos);
			$this->set('mostViewed', $mostViewed);			
			$this->set('tags', $tags);			
			$this->set('album', $album);
		}
        
        public function freevideos(){
            $vimeo = $this->VimeoAuth->doAuth();
            $username = $this->VimeoAuth->getUsername();
            $mostViewed = $this->VimeoVideo->getMostViewedVideos($username, 5, $vimeo);	
            $this->set('videos', $this->VimeoVideo->getAllFreeVideos($vimeo));
            $this->set('mostViewed', $mostViewed);	
        }
		
		private function getUserRemainingToken(){
			$this->loadModel('User');
			$user = $this->User->findById($this->Session->read('Users.Id'));
            $remainingTokens = 0;
            if (isset($user['User'])) {
                $remainingTokens = $user['User']['RemainingTokens'];
            }
			return $remainingTokens;
		}
		
		private function getVideoAlbums($videoId = null){
			$this->loadModel('AlbumsVideo');
			$albums = $this->AlbumsVideo->find('first', array('conditions' => array('VideoId' => $videoId)));
			return $albums['AlbumsVideo']['AlbumId'];
		}
			
	}
	
?>