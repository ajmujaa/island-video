<?php 
	App::uses('Component', 'Controller');
	
	class UserComponent extends Component{
	
		public function getSingleUser($id = null){
			$model = ClassRegistry::init('User');
			$user = $model->findById($id);
			return $user;
		}
		
	}
		
?>