<?php 
	App::uses('Component', 'Controller');
	
	class VimeoVideoComponent extends Component{
		
		public $components = array('Category');
		
		public function getAllVideos($userId, $vimeoAuth){
			$videos = $vimeoAuth->call('vimeo.videos.getAll', array('user_id' => $userId, 'full_response' => true));
			$vmvideo = $videos->videos->video;
			$localVideoIds = $this->getAllLocalVideos();
			$authors = $this->getVideoDetails();
			foreach($vmvideo as $v){
				foreach($authors as $author){
					if(!in_array($v->id, $localVideoIds)){
						unset($v);
					}else if($v->id === $author['Video']['Id']){
						$v->author_id = $author['Video']['AuthorId'];	
						$v->author_name = $author['User']['FullName'];
					}
				}
			}
			return $vmvideo;
		}
		
		public function getAllVimeoVideo($vimeoUsername, $vimeoAuth, $userId){
			$videos = $vimeoAuth->call('vimeo.videos.getAll', array('user_id' => $vimeoUsername));	
			$vmvideo = $videos->videos->video;			
			$usersVideoIds = $this->getAllUserVideosByUserId($userId);
			$filteredVideos = array();
			foreach($vmvideo as $v){
				if(!in_array($v->id, $usersVideoIds)){
					unset($v);
				}else{
					$filteredVideos[] = $v;	
				}
			}
			return $filteredVideos;
		}
        
        public function getAllFreeVideos($vimeoAuth){
            $model = ClassRegistry::init('Video');   
            $freevideos = $model->find('all', array('conditions' => array('VideotypeId' => '1')));
            $videos = array();
            foreach($freevideos as $freevideo){
                $videos[] = $this->getVimeoVideo($freevideo['Video']['Id'], $vimeoAuth);        
            }
            return $videos;
        }
		
		public function getVimeoVideo($id = null, $vimeoAuth){
			$video = $vimeoAuth->call('vimeo.videos.getInfo', array('video_id' => $id));
			$author = $this->getAuthorByVideo($id);
			$video->video[0]->author_id = $author[0]['Video']['AuthorId'];
			$video->video[0]->author_name = $author[0]['User']['FullName'];
			$video->video[0]->type = $author[0]['Videotype']['Type'];
			$video->video[0]->token = $author[0]['Video']['TokenValue'];
			$video->video[0]->channel_id = $author[0]['Video']['ChannelId'];
			return $video->video;
		}	
        
        public function getVimeoVideoWithAlbum($id = null, $vimeoAuth, $albumId){
			$video = $vimeoAuth->call('vimeo.videos.getInfo', array('video_id' => $id));
			$author = $this->getAuthorByVideo($id);
            $album = $this->Category->getAlbumName($albumId);
			$video->video[0]->author_id = $author[0]['Video']['AuthorId'];
			$video->video[0]->author_name = $author[0]['User']['FullName'];
			$video->video[0]->type = $author[0]['Videotype']['Type'];
			$video->video[0]->token = $author[0]['Video']['TokenValue'];
			$video->video[0]->channel_id = $author[0]['Video']['ChannelId'];
            $video->video[0]->album_id = $albumId;
            $video->video[0]->album_name = $album;
			return $video->video;
		}	
		
		public function getRecentlyUploadedVideos($username, $numberofItems, $page, $vimeoAuth){
			$videos = $vimeoAuth->call('vimeo.videos.getAll', array('user_id' => $username, 'page' => $page, 'per_page' => $numberofItems, 'sort' => 'newest', 'full_response' => true));	
			return $videos->videos->video;
		}
		
		public function getChannels($userId, $vimeoAuth){
			$channels = $vimeoAuth->call('vimeo.channels.getAll', array('user_id' => $userId, 'full_summary' => true));
			return $channels->channels->channel;
		}
		
		public function getAlbums($userId, $vimeoAuth){
			$albums = $vimeoAuth->call('vimeo.albums.getAll', array('user_id' => $userId));
			$authors = $this->getVideoDetails();
			$video = array();
			$vmvideo = array();
			foreach($albums->albums->album as $album){
				$video = $this->getVideosByAlbum($album->id, $vimeoAuth);	
				$video['album_id'] = $album->id;
				$video['album_title'] = $album->title;
				foreach($video as $v){
					if(!empty($v->id)){
						foreach($authors as $author){
							if($v->id === $author['Video']['Id']){
								$v->author_id = $author['Video']['AuthorId'];	
								$v->author_name = $author['User']['FullName'];
								$v->type = $author['Videotype']['Type'];
							}
						}	
					}
				}
				$vmvideo[] = $video;
			}
			return $vmvideo;	
		}
		
		public function getVideosByAlbum($albumId, $vimeoAuth){
			$model = ClassRegistry::init('AlbumsVideo');
			$localvideos = $model->find('all', array('conditions' => array('AlbumId' => $albumId)));
			$albumvideos = array();
			foreach($localvideos as $lv){
				$albumvideos[] = $this->getVimeoVideoWithAlbum($lv['AlbumsVideo']['VideoId'], $vimeoAuth, $albumId);
			}
			return $this->object_array_unique($albumvideos);
		}

        public function getVideosByChannelGroupAndAlbum($channelId, $groupId, $albumId, $vimeoAuth){
			$albumModel = ClassRegistry::init('AlbumsVideo');
            $groupModel = ClassRegistry::init('GroupsVideo');
            $channelModel = ClassRegistry::init('Videos');
            
            $localchannelvideos = $channelModel->find('all', array('conditions' => array('ChannelId' => $channelId)));
			$localalbumvideos = $albumModel->find('all', array('conditions' => array('AlbumId' => $albumId)));
            $localgroupsvideos = $groupModel->find('all', array('conditions' => array('GroupId' => $groupId)));
            
          
            $albumsCount = count($localalbumvideos);
            $groupsCount = count($localgroupsvideos);
            $videoIdsArray = array();
            if($albumsCount > $groupsCount){
                foreach($localalbumvideos as $localalbum){
                    foreach($localgroupsvideos as $localgroup) {
                        if($localalbum['AlbumsVideo']['VideoId'] === $localgroup['GroupsVideo']['VideoId']){
                            $videoIdsArray = $localalbum['AlbumsVideo']['VideoId'];   
                        }
                    }
                }
            }
            
            $albumvideos = array();
            if(!empty($localalbumvideos) && !empty($localgroupsvideos)){
                foreach($localalbumvideos as $lv){
                    $albumvideos[] = $this->getVimeoVideoWithAlbum($lv['AlbumsVideo']['VideoId'], $vimeoAuth, $albumId);
                }
            }
			return $this->object_array_unique($albumvideos);
		}
		
        public function getVideosByGroupAndAlbum($groupId, $albumId, $vimeoAuth){
			$albumModel = ClassRegistry::init('AlbumsVideo');
            $groupModel = ClassRegistry::init('GroupsVideo');
			$localalbumvideos = $albumModel->find('all', array('conditions' => array('AlbumId' => $albumId)));
            $localgroupsvideos = $groupModel->find('all', array('conditions' => array('GroupId' => $groupId)));
            $albumvideos = array();
            if(!empty($localalbumvideos) && !empty($localgroupsvideos)){
                foreach($localalbumvideos as $lv){
                    $albumvideos[] = $this->getVimeoVideoWithAlbum($lv['AlbumsVideo']['VideoId'], $vimeoAuth, $albumId);
                }
            }
			return $this->object_array_unique($albumvideos);
		}
				
		public function getGroups($userId, $vimeoAuth){
			$groups = $vimeoAuth->call('vimeo.groups.getAll', array('user_id' => $userId));	
			return $groups->groups->group;
		}
		
		public function getAllVideosByChannel($userId, $vimeoAuth){
			$videos = $vimeoAuth->call('vimeo.videos.getAll', array('user_id' => $userId,'full_response' => true));
			$vmvideo = $videos->videos->video;
			$localVideoIds = $this->getAllLocalVideos();
			$vids = $this->getVideoDetails();
			for($i = 0; $i < count($vmvideo); $i++){
				foreach($vids as $video){
                    if(!in_array($vmvideo[$i]->id, $localVideoIds)){
						unset($vmvideo[$i]);
                    }else if($vmvideo[$i]->id === $video['Video']['Id']){
						$vmvideo[$i]->author_id = $video['Video']['AuthorId'];	
						$vmvideo[$i]->author_name = $video['User']['FullName'];
						$vmvideo[$i]->type = $video['Videotype']['Type'];
						$vmvideo[$i]->channel_id = $video['Video']['ChannelId'];
						$vmvideo[$i]->channel_name = $video['Video']['ChannelId'] === 0 ? $this->Category->getChannelName($video['Video']['ChannelId']) : '';	
						$vmvideo[$i]->album_id = $this->getAlbumsForVideo($vmvideo[$i]->id);
					}
				}
			}
            
			return $vmvideo;
		}
		
		public function getAlbumsForVideo($videoId){
			$model = ClassRegistry::init('AlbumsVideo');
			$album = $model->find('all', array('conditions' => array('VideoId' => $videoId)));	
			if(!isset($album)){
				return ($album[0]['AlbumsVideo']['AlbumId']);	
			}
		}
		
		public function getAllVideosByGroup($userId, $vimeoAuth){
			$videos = $vimeoAuth->call('vimeo.videos.getAll', array('user_id' => $userId,'full_response' => true));
			$vmvideo = $videos->videos->video;
			$localVideoIds = $this->getAllLocalVideos();
			$vids = $this->getVideoDetails();
			for($i = 0; $i < count($vmvideo); $i++){
				foreach($vids as $video){
					if(!in_array($vmvideo[$i]->id, $localVideoIds)){
						unset($vmvideo[$i]);
					}else if($vmvideo[$i]->id === $video['Video']['Id']){
						$vmvideo[$i]->author_id = $video['Video']['AuthorId'];	
						$vmvideo[$i]->author_name = $video['User']['FullName'];
						$vmvideo[$i]->type = $video['Videotype']['Type'];
					}
				}
			}
			return $vmvideo;	
		}
		
		public function getRelatedVideosByAlbum($albumId = null, $vimeoAuth){
			$model = ClassRegistry::init('Video');
			$videos = $model->find('all', array('conditions'=>array('AlbumId' => $albumId), 'fields'=>array('Id')));
			$videoIds = array();
			$relatedVideos = array();
			foreach($videos as $video){
				$videoIds[] = $video['Video']['Id'];	
			}
			foreach($videoIds as $videoId){
				$relatedVideos[] = $this->getVimeoVideo($videoId, $vimeoAuth);	
			}
			return $relatedVideos;						
		}
		
		public function getAlbumsForGroup(){
			$albums = $this->Category->getAllAlbums();
			return $this->object_array_unique($albums);
		}
				
		public function getGroupsForChannel(){
			$groups = $this->Category->getAllGroups();
			return $this->object_array_unique($groups);
		}
		
		public function getAlbumsForGroups($groupId){
			$albums = $this->Category->getAlbumsByGroupId($groupId);
			$groupAlbums = array();
			foreach($albums as $album){
                $albumName = $this->Category->getAlbumName($album['ChannelsGroupsAlbum']['Album_Id']);
                $groupAlbums[] = array($album['ChannelsGroupsAlbum']['Album_Id'], $albumName);   
			}
			return $this->object_array_unique($groupAlbums);
		}
        
        public function getAlbumsByGroupsAndChannel($groupId, $channelId){
            $albums = $this->Category->getAlbumsByGroupAndchannel($groupId, $channelId);   
            $groupAlbums = array();
			foreach($albums as $album){
                $albumName = $this->Category->getAlbumName($album['ChannelsGroupsAlbum']['Album_Id']);
                $groupAlbums[] = array($album['ChannelsGroupsAlbum']['Album_Id'], $albumName);   
			}
			return $this->object_array_unique($groupAlbums);
        }
		
		public function getVideosByChannel($userId, $vimeoAuth){
			$channels = $this->getChannels($userId, $vimeoAuth);
			$channelVideo = array();
			foreach ($channels as $channel){
				$channelVideo[] = $this->getAllVideosByChannel($userId, $channel->id, $vimeoAuth);	
			}
			return $channelVideo;
		}
		
		public function getSimilarVideosByTag($userId, $vimeoAuth, $videoId){
			$tags = $this->getCurrentVideoTags($videoId, $vimeoAuth);
			$videos = $this->getAllVideosForTags($userId, $tags, $vimeoAuth);
			return $this->object_array_unique($videos);
		}	
		
		public function getCurrentVideoTags($videoId, $vimeoAuth){
			$currentVideo = $this->getVimeoVideo($videoId, $vimeoAuth);
			$currentVideoTags = $currentVideo[0]->tags->tag;
			$tags = array();
			foreach($currentVideoTags as $vtag){
				$tags[] = $vtag->normalized;	
			}
			return $tags;
		}
		
		public function getAllVideosForTags($userId, $tags, $vimeoAuth){
			$videos = array();
			$allVideos = $this->getAllVideos($userId, $vimeoAuth);	
			foreach($allVideos as $tvideos){
				foreach($tvideos->tags->tag as $t){
					if(in_array($t->normalized, $tags)){
						$videos[] = $tvideos;	
					}
				}
			}
			return $videos;
		}
		
		
		public function getAllTags($userId, $vimeoAuth){
			$tags = array();
			$allVideos = $this->getAllVideos($userId, $vimeoAuth);	
			foreach($allVideos as $video){
				foreach($video->tags->tag as $tag){
					$tags[] = $tag->normalized;	
				}
			}
			return $this->object_array_unique($tags);
		}
		
		public function getVideosByTag($userId, $vimeoAuth, $tag){
			$a = array();
			$tags[] = $tag;
			$videos = $this->getAllVideosForTags($userId, $tags, $vimeoAuth);	
			return $videos;
		}
		
		public function getMostViewedVideos($userId, $noOfVideos, $vimeoAuth){
			$videos = $vimeoAuth->call('vimeo.videos.getAll', array('user_id' => $userId, 'sort' => 'most_played', 'per_page' => $noOfVideos, 'summary_response' => true));
			return $videos->videos->video;
		}	
		
		public function getAllLocalVideos(){
			$model = ClassRegistry::init('Video');
			$videos = $model->find('all', array('field' => 'Id'));
			$videoIds = array();
			foreach($videos as $video){
				$videoIds[] = $video['Video']['Id'];	
			}
			return $videoIds;
		}
		
		public function getAllUserVideosByUserId($userId = null){
			$model = ClassRegistry::init('Usersvideo');
			$videos = $model->find('all', array('field' => 'VideosId', 'condition' => array('UsersId' => $userId)));
			$videoIds = array();
			foreach($videos as $video){
				$videoIds[] = $video['Usersvideo']['VideosId'];	
			}
			return $videoIds;	
		}
		
		public function getVideosByUserId($userId = null, $vimeoAuth){
			$videoIds = $this->getAllUserVideosByUserId($userId);
			$videos = array();
			foreach($videoIds as $videoId){
				$videos[] = $this->getVimeoVideo($videoId, $vimeoAuth);
			}
			return $videos;
		}
		
		public function getVideoDetails(){
			$model = ClassRegistry::init('Video');
			$groupmodel = ClassRegistry::init('GroupsVideo');
			$albummodel = ClassRegistry::init('AlbumsVideo');
			$videos = $model->find('all', array('fields' => array('Id', 'AuthorId', 'User.FullName', 'Videotype.Type', 'TokenValue', 'ChannelId', 'GroupId', 'AlbumId')));

			
			$vids = array();	
			foreach($videos as $video){
				$vids[] = $video;	
			}
			return $vids;
		}
		
		public function getAuthorByVideo($id = null){
			$model = ClassRegistry::init('Video');
			$author = $model->find('all', array('conditions' => array('Video.Id' => $id)));
			return $author;
		}
        
        public function getAuthorByVideoId($id = null){
            $model = ClassRegistry::init('Video');
			$author = $model->find('all', array('conditions' => array('Video.Id' => $id)));
			return $author[0]['User'];
        }
        
        public function getAuthorsbyVideos($videoIds){
            $model = ClassRegistry::init('Video');
            $authors = array();
            foreach($videoIds as $videoId){
                $authors[] = $this->getAuthorByVideoId($videoId);
            }
            return $this->object_array_unique($authors);
               
        }
		
		
		public function object_array_unique($array, $keep_key_assoc = false){
			$duplicate_keys = array();
			$tmp = array();       
			foreach ($array as $key=>$val){
				// convert objects to arrays, in_array() does not support objects
				if (is_object($val)){
					$val = (array)$val;
				}
				if (!in_array($val, $tmp)){
					$tmp[] = $val;
				}else{
					$duplicate_keys[] = $key;
				}
			}
			foreach ($duplicate_keys as $key){
				unset($array[$key]);
			}
			return $keep_key_assoc ? $array : array_values($array);
		}
		
	}
?>
