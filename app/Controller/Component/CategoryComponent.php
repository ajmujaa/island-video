<?php 
	App::uses('Component', 'Controller');
	
	class CategoryComponent extends Component{
		
		public function getAllChannels(){
			$model = ClassRegistry::init('Channel');
			return $model->find('all', array('fields' => array('Id', 'Name')));	
		}
		
		public function getChannelName($id){
			$model = ClassRegistry::init('Channel');
			$channel = $model->findById($id);
			if(isset($channel)){
                return $channel['Channel']['Name'];	
            }
		}
		
		public function getAllGroups(){
			$model = ClassRegistry::init('Group');;
			return $model->find('all' , array('fields' => array('Id', 'Name')));
		}
        
        public function filteredAllGroups(){
            $model = ClassRegistry::init('Group');
            $group = array();
            foreach($this->getAllChannelGroups() as $a){
                if($a['ChannelsGroupsAlbum']['Album_Id'] !== '0'){
                    $group[] = array($a['ChannelsGroupsAlbum']['Group_Id'], $this->getGroupName($a['ChannelsGroupsAlbum']['Group_Id']));
                }
            }
            return $this->object_array_unique($group);
        }
        
        public function getAllChannelGroups(){
            $model = ClassRegistry::init('ChannelsGroupsAlbum');
            return $this->object_array_unique($model->find('all' , array('fields' => array('Group_Id', 'Channel_Id', 'Album_Id'), 'conditions' => array('Album_Id != ' => 0 ))));
        }
        
        public function getAllGroupsUnfiltered(){
            $model = ClassRegistry::init('ChannelsGroupsAlbum');
            return $this->object_array_unique($model->find('all' , array('fields' => array('Group_Id', 'Channel_Id', 'Album_Id'))));
            
        }
		
		public function getGroupName($id = null){
			$model = ClassRegistry::init('Group');
			$group = $model->findById($id);	
			return $group['Group']['Name'];
		}
		
		public function getAllAlbums(){
			$model = ClassRegistry::init('Album');
			return $model->find('all', array('fields' => array('Id', 'Name')));
		}
        
        public function getAllChannelsAlbum(){
            $model = ClassRegistry::init('ChannelsAlbum');
			return $model->find('all', array('fields' => array('Album_Id', 'Channel_Id')));
        }
		
		public function getAllChannelsGroupsForAlbums(){
			$model = ClassRegistry::init('ChannelsGroupsAlbum');
			return $model->find('all');
		}
		
		public function getAlbumName($id = null){
			$model = ClassRegistry::init('Album');
			$album = $model->findById($id);
			return $album['Album']['Name'];	
		}
		
		public function getAlbumsByGroupId($id = null){
			$model = ClassRegistry::init('ChannelsGroupsAlbum');
			$album = $model->find('all', array('conditions' => array('Group_Id' => $id)));
			return $album;
		}
        
        public function getAlbumsByGroupAndchannel($groupId, $channelId){
            $model = ClassRegistry::init('ChannelsGroupsAlbum');
            $items = $model->find('all', array('fields' => array('Group_Id', 'Channel_Id', 'Album_Id'), 'conditions' => array('Group_Id' => $groupId, 'Channel_Id' => $channelId)));
            return $this->object_array_unique($items);
        }
		
		public function getRequestedItem($type, $id = null){
			$model = ClassRegistry::init($type);
			$item = $model->findById($id);
            debug($item);
			return $item;
		}
		
		public function getRequestedList($type, $field){
			$model = ClassRegistry::init($type);
			$items = $model->find('list' , array('fields' => array('Id', $field)));
			return $items;
		}
		
		public function object_array_unique($array, $keep_key_assoc = false){
			$duplicate_keys = array();
			$tmp = array();       
			foreach ($array as $key=>$val){
				// convert objects to arrays, in_array() does not support objects
				if (is_object($val)){
					$val = (array)$val;
				}
				if (!in_array($val, $tmp)){
					$tmp[] = $val;
				}else{
					$duplicate_keys[] = $key;
				}
			}
			foreach ($duplicate_keys as $key){
				unset($array[$key]);
			}
			return $keep_key_assoc ? $array : array_values($array);
		}
		
			
	}
?>