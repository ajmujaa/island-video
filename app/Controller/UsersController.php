<?php
	App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 */
class UsersController extends AppController {

/**
 * Scaffold
 *
 * @var mixed
 */
	public $scaffold;
	public $helpers = array('Html', 'Form');
	public $components = array('Session', 'VimeoAuth', 'VimeoVideo');
	
	
	public function view($id = null){
		$vimeo = $this->VimeoAuth->doAuth();
		if(!$id){
			throw new NotFoundException(__('Invalid User'));	
		}
		$user = $this->User->findById($id);
		if(!$user){
			throw new NotFoundException(__('Invalid User'));
		}
		$this->set('user', $user);
		$this->set('videos', $this->VimeoVideo->getVideosByUserId($id, $vimeo));		
	}
}
