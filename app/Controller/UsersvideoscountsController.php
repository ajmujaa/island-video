<?php 
	
	App::uses('AppController', 'Controller');
	App::import('Vendor', 'Vimeo/phpVimeo');
	
	class UsersvideoscountsController extends AppController{
		
		public function add($videoId = null){
			$this->request->data['Usersvideoscount']['UsersId'] = $this->Session->read('Users.Id');
			$this->request->data['Usersvideoscount']['VideosId'] = $videoId;
			$this->request->data['Usersvideoscount']['Count'] = "1";
			$this->Usersvideoscount->create();
			if($this->request->is('post')){
				if($this->Usersvideoscount->save($this->request->data)){
					$this->getVideoToken($videoId);
				}else{
					echo "Fail";	
				}
			}
			$this->autoRender = false;
		}
		
		private function getVideoToken($videoId = null){
			$this->loadModel('Video');
			if(!$videoId){
				throw new NotFoundException(__('Invalid Video ID'));		
			}
			$video = $this->Video->findById($videoId);
			$this->deductTokenFromUser($this->Session->read('Users.Id'), $video['Video']['TokenValue']);
		}
		
		private function deductTokenFromUser($userId = null, $token = null){
			$this->loadModel('User');
			if(!$userId){
				throw new NotFoundException(__('Invalid User ID'));
			}
			$user = $this->User->findById($userId);
			$deductToken = $user['User']['RemainingTokens'] - $token;
			
			$this->User->id= $userId;
			$this->request->data['User']['RemainingTokens'] = $deductToken;
			if($this->User->save($this->request->data)){
				echo "Success";	
			}
		}
			
	}

?>