$(document).foundation();

// Show hide script for paid and free videos
$(document).ready(function(){
	
	$('.video-overlay').width($("#largeplayer").width());
	$('.video-overlay').height($("#largeplayer").height());
	
	if($('.video-overlay').data('access') === "noaccess"){
		$(".vimeo").css('visibility', 'hidden');	
	}
	
	$('.yes').click(function(){
		var id = $('.yes').data('id'),
			url = "/islandvideo/Usersvideoscounts/add/" + id + "";
		$.ajax({
			type: 'POST',
			url: url,
			success: function(data){
				if(data === "Success"){
					$('.video-overlay').hide();	
				}
			}
		});
		
	});	
	
	$('.no').click(function(){
		window.location.href = "http://daybridgeinternationalschool.com/islandvideo";
	});
		
	$('.play-icon').click(function(){
		alert("Playing");	
	});	
	
	$("#paid-video-content").hide();

	$("#link-free-video").click(function(){
		$("#free-video-content").slideDown();
	});
	$("#link-paid-video").click(function(){
		$("#paid-video-content").slideDown();
	});

    $("#categories-link").click(function(){
		$("#paid-video-content").slideDown();
	});
	
		   
	$("#link-free-video").click(function(){
		$("#paid-video-content").hide();
	});
	$("#link-paid-video").click(function(){
		$("#free-video-content").hide();
	});
    $("#categories-link").click(function(){
		$("#free-video-content").hide();
	});
	
	$('.slider5').bxSlider({
		slideWidth: 200,
		minSlides: 8,
		maxSlides: 8,
		moveSlides: 8,
		slideMargin: 20
	  });
	  
	  $('.slider').bxSlider({
		slideWidth: 200,
		minSlides: 4,
		maxSlides: 4,
		moveSlides: 4,
		slideMargin: 20
	  });
	 
/* ============= Add the categories to the combobox when its more than 4 ============= */

	var $items = $('.items');

	$.each($items, function($item){
		var id = $item + 1,
			itemId = '.items#items' + id + '',
			items = '.items#items' + id + ' .item',
			categId = '#categ' + id + '',
			$listItems = $(items),
			$select = $(categId);
			
		$select.append('<option>Select One</option>');				
		$.each($listItems, function(idx,obj){
		  var $option = $("<option>"+ $(obj).text() +"</option>").val( $(obj).find('a').attr('href') );
		  if ( $(obj).hasClass('active') ){
			$option.attr('selected', 'selected');
		  }
		  $select.append($option);
		});
		$select.find('option').length <= 5 ? $select.hide() : $select.show();	
		$(items + ':gt(3)').hide();		
	});

   // Go to page on select.
	$('.categ').change(function(){
	  var selected = $(this).find('option:selected');
	  window.location.href = selected.val();
	});
  
  /* ================ Category combox box done ===================== */
   
	
	
}); 

function elementCount(){
	var count = $(".categories > .items").size();	
	return count;
}
